module Reviews
  class Create < BaseService
    include TaskRequestValidations
    include TaskValidations
    include ReviewValidations
    include UserValidations

    attr_reader :current_user, :params, :review, :task

    def initialize(current_user, params)
      @current_user, @params = current_user, params
      @task = Task.find_by(id: params[:task_id])
      @review = Review.new(sender_id: current_user.id)
    end

    def call
      validate!
      review.assign_attributes(params.merge({recipient_id: task.user_id}))
      save_and_return(review) do
        increase_rating(task.user, review.rating) # @todo нужно ли рефакторить
      end
    end

    private

    def validate!
      user_active?(current_user)
      task_completed?(task)
      user_related_with_task?(current_user, task)
      review_already_exist?(current_user, task)
      task_has_valid_time?(task)
      task_request = task.task_requests.find_by(user_id: current_user.id)
      user_owner_request?(current_user, task_request)
      task_request_completed?(task_request)
    end
  end
end