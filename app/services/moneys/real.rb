module Moneys
  class Real < Moneys::Base
    def increment
      validate!
      update_score(user.real_money, :increment, amount, commit)
    end

    def decrement
      validate!
      money_in_score?(user.real_money.amount, amount)
      update_score(user.real_money, :decrement, amount, commit)
    end
  end
end