class Admin::CitiesController < AdminController
  before_action :get_city, only: [:edit, :update, :destroy, :restore]

  def index
    @cities = City.unscoped.all.includes(:users, :tasks).order(:position)
  end

  def edit; end

  def update
    @city.assign_attributes(city_params)
    @city.save ? redirect_to(edit_admin_city_path(@city)) : redirect_back(admin_cities_path)
  end

  def destroy
    @city.update_attribute(:deleted, true)
    redirect_to(admin_cities_path)
  end

  def restore
    @city.update_attribute(:deleted, false)
    redirect_to(admin_cities_path)
  end

  def new
    @city = City.new
  end

  def create
    city = City.new(city_params)
    city.save ? redirect_to(edit_admin_city_path(city)) : redirect_back(new_admin_city_path)
  end

  private

  def city_params
    params.require(:city).permit(:title, :meta_title, :meta_description, :meta_keywords)
  end

  def get_city
    @city = City.unscoped.find(params[:id])
  end
end