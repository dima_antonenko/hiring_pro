require 'rails_helper'

RSpec.describe Api::V1::CitiesController, type: :request do
  let! (:city) { create(:city) }

  def get_cities
    get api_v1_cities_path
    JSON.parse(response.body)
  end

  # get all cities
  describe 'GET /api/v1/cities' do
    it 'User can get all cities | response has valid http status' do
      get_cities
      expect(response).to have_http_status(200)
    end

    it 'User can get all cities | response has valid data' do
      json = get_cities
      expect(json['data'][0]['attributes']['title']).to eq(city.title)
    end
  end
end