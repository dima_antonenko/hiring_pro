FactoryBot.define do
  factory :task do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.sentence(word_count: 10) }
    price { 1000 }
    start_date { DateTime.now + 2.days }
    completion_date { (DateTime.now + 5.days).strftime('%a, %d %b %Y %H:%M:%S') }
    payment_type { 0 }
    completed_at { DateTime.now + 1.day }
    service_location { "My home" }
    category
    user
    city
  end
end
