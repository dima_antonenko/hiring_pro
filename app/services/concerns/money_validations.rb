module MoneyValidations
  MAX_DIFF_AMOUNT = 100_00
  MIN_DIFF_AMOUNT = 1

  def amount_valid?(amount)
    raise ForbiddenError.new(:amount, 'Amount invalid') if amount > MAX_DIFF_AMOUNT || amount < MIN_DIFF_AMOUNT
  end

  def money_in_score?(score, amount)
    raise ForbiddenError.new(:amount, 'Invalid value in money') if score < amount
  end
end