class City < ApplicationRecord
  has_many :users
  has_many :tasks

  default_scope { where.not(deleted: true) }
end
