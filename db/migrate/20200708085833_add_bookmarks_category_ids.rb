class AddBookmarksCategoryIds < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :bookmark_category_ids, :integer, index: true, null: false,
               array: true, default: []

    add_column :users, :bookmark_city_ids, :integer, index: true, null: false,
               array: true, default: []
  end
end
