FactoryBot.define do
  factory :task_request do
    task
    user
    price { 1000 }
    message { Faker::Movies::HarryPotter.quote[0..399]  }
  end
end
