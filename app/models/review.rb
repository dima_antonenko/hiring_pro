class Review < ApplicationRecord
  belongs_to :task
  belongs_to :user, foreign_key: "recipient_id"

  enum state: { active: 0 }

  def creator
    User.find_by(id: self.sender_id)
  end

  def recipient
    User.find_by(id: self.recipient_id)
  end
end
