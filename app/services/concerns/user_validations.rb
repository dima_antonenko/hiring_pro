module UserValidations
  def user_active?(user)
    raise PermissionError.new(:state, 'User not autorized') if !user || user.deleted
  end
end