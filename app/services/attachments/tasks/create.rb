module Attachments
  module Tasks
    class Create < Attachments::Base
      attr_reader :current_user, :task, :attachments

      def initialize(current_user, task_id, attachments)
        @current_user, @attachments = current_user, attachments # без понятия почему так
        @task = Task.find_by(id: task_id)
      end

      def call
        validate!
        attachments.each do |a|
          task.attachments.attach(
            io: a,
            filename: 'attachment.jpg'
            )
        end
        save_and_return(task)
      end

      private

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        attachments_exist?(attachments)
        user_active?(current_user)
        task_not_deleted?(task)
        task_has_valid_completion?(task)
        user_has_access?(current_user, task)
        task_not_approved?(task)
      end
    end
  end
end
