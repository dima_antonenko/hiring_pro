# category of tasks
class Category < ApplicationRecord
  acts_as_nested_set

  alias :categories :children
  alias :category_ids :child_ids

  has_many :tasks
end
