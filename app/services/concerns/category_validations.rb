module CategoryValidations
  def category_exist?(category)
    raise PermissionError.new(:category_id, 'Category not exist') unless category
  end

  def category_not_deleted?(category)
    raise PermissionError.new(:category_id, 'Category must be not deleted') if category.deleted
  end

  def category_not_root?(category)
    raise PermissionError.new(:category_id, 'Category cant be root') if category.root?
  end
end
