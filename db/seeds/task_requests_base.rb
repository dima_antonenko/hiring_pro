@request_creator       = User.find_by(email: 'request_creator@test.ru')
@other_request_creator = User.find_by(email: 'other_request_creator@test.ru')

@completed_task = Task.find_by(title: 'Completed task')
@not_completed_task = Task.find_by(title: 'Task with approved requests')

def create_request(current_user, task)
  prm = {
      task_id: task.id,
      price: 2000,
      message: 'This is task_request message'
  }
  task_request = TaskRequests::Create.new(current_user, prm).call
  puts "-Added task_request: #{task_request.attributes}"
end

TaskRequest.destroy_all

create_request(@request_creator, @completed_task)
create_request(@other_request_creator, @completed_task)
puts "Added task_requests for task: #{@completed_task.title} \n \n"

create_request(@request_creator, @not_completed_task)
create_request(@other_request_creator, @not_completed_task)
puts "Added task_requests for task: #{@not_completed_task.title} \n \n"



