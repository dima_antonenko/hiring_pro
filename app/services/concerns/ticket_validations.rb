module TicketValidations
  MAX_TICKETS_TODAY = 5

  def ticket_active?(ticket)
    raise PermissionError.new(:state,'This ticket not active') if !ticket || ticket.deleted?
  end

  def user_related_to_ticket?(ticket, user)
    raise PermissionError.new(:user_id, 'User not related to his ticket') if ticket.user_id != user.id
  end

  def user_ticket_limits_valid?(user)
    raise PermissionError.new(:user_id,'user ticket limits not exceeded') if user_tickets_today(user.id) > MAX_TICKETS_TODAY
  end

  private

  def user_tickets_today(user_id)
    Ticket.where(user_id: user_id, created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).size
  end
end