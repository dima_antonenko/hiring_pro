class TicketMessageSerializer
  include FastJsonapi::ObjectSerializer
  attributes :content

  belongs_to :user
  belongs_to :ticket
end