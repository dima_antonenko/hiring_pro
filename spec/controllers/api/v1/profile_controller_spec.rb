require 'rails_helper'
RSpec.describe Api::V1::ProfileController, type: :request do
  let! (:user) { create(:user) }

  def update_user(user, extra_params = nil)
    user_params = {name: 'New user name'}
    user_params.merge!(extra_params) if extra_params
    post update_profile_path, params: {user: user_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_content_limits
    get profile_content_limits_path, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_user
    get me_api_v1_users_path, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # update user profile
  describe 'POST /api/v1/profile/update' do
    it 'user can update his profile' do
      json = update_user(user)
      expect(json['data']['attributes']['name']).to eq('New user name')
    end
  end

  describe 'GET /api/v1/profile/content_limits' do
    it 'user can get settings of content limits' do
      json = get_content_limits
      expect(json['limits']['task']['max_execution_time']).to be_truthy
    end
  end


  describe 'GET /api/v1/profile/me' do
    context 'real' do
      context 'increment' do
        before(:each) do
          Moneys::Real.new(user, 100).increment
        end

        it 'Money#amount' do
          json = get_user
          expect(json['data']['attributes']['real_money']).to eq('100.0')
        end

        it 'MoneyLog#amount_before' do
          log = user.real_money.money_logs.first
          expect(log.amount_before).to eq(0.0)
        end

        it 'MoneyLog#amount_after' do
          log = user.real_money.money_logs.first
          expect(log.amount_after).to eq(100.0)
        end

        it 'MoneyLog#amount_diff' do
          log = user.real_money.money_logs.first
          expect(log.amount_diff).to eq(100.0)
        end
      end

      context 'decrement' do
        before(:each) do
          Moneys::Real.new(user, 100).increment
          Moneys::Real.new(user, 55).decrement
        end

        it 'Money#amount' do
          json = get_user
          expect(json['data']['attributes']['real_money']).to eq('45.0')
        end

        it 'Money#amount | cant decrement more than 0' do
          expect{
            Moneys::Real.new(user, 55).decrement
          }.to raise_error(ForbiddenError)
        end

        it 'MoneyLog#amount_before' do
          log = user.real_money.money_logs.last
          expect(log.amount_before).to eq(100.0)
        end

        it 'MoneyLog#amount_after' do
          log = user.real_money.money_logs.last
          expect(log.amount_after).to eq(45.0)
        end

        it 'MoneyLog#amount_diff' do
          log = user.real_money.money_logs.last
          expect(log.amount_diff).to eq(-55.0)
        end
      end
    end
    context 'virtual' do
      context 'increment' do
        before(:each) do
          Moneys::Virtual.new(user, 100).increment
        end

        it 'Money#amount' do
          json = get_user
          expect(json['data']['attributes']['virtual_money']).to eq('100.0')
        end

        it 'MoneyLog#amount_before' do
          log = user.virtual_money.money_logs.first
          expect(log.amount_before).to eq(0.0)
        end

        it 'MoneyLog#amount_after' do
          log = user.virtual_money.money_logs.first
          expect(log.amount_after).to eq(100.0)
        end

        it 'MoneyLog#amount_diff' do
          log = user.virtual_money.money_logs.first
          expect(log.amount_diff).to eq(100.0)
        end
      end

      context 'decrement' do
        before(:each) do
          Moneys::Virtual.new(user, 100).increment
          Moneys::Virtual.new(user, 55).decrement
        end

        it 'Money#amount' do
          json = get_user
          expect(json['data']['attributes']['virtual_money']).to eq('45.0')
        end

        it 'Money#amount | cant decrement more than 0' do
          expect{
            Moneys::Virtual.new(user, 55).decrement
          }.to raise_error(ForbiddenError)
        end

        it 'MoneyLog#amount_before' do
          log = user.virtual_money.money_logs.last
          expect(log.amount_before).to eq(100.0)
        end

        it 'MoneyLog#amount_after' do
          log = user.virtual_money.money_logs.last
          expect(log.amount_after).to eq(45.0)
        end

        it 'MoneyLog#amount_diff' do
          log = user.virtual_money.money_logs.last
          expect(log.amount_diff).to eq(-55.0)
        end
      end
    end
  end
end