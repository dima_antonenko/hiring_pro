module Profile
  class Update < BaseService
    attr_reader :current_user, :params

    def initialize(current_user, params)
      @current_user, @params = current_user, params
    end

    def call
      current_user.assign_attributes(params)
      save_and_return(current_user)
    end
  end
end