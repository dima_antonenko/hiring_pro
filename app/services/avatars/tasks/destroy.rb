module Avatars
  module Tasks
    class Destroy < Avatars::Base
      attr_reader :current_user, :task

      def initialize(current_user, task_id)
        @current_user, @task = current_user, Task.find_by(id: task_id)
      end

      def call
        validate!
        task.avatar.purge
        save_and_return(task)
      end

      private

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        task_not_deleted?(task)
        user_active?(current_user)
        task_has_valid_completion?(task)
        user_has_access?(current_user, task)
        task_not_approved?(task)
        raise PermissionError.new('image not attached') unless task.avatar.attached?
      end
    end
  end
end