module Bookmarks
  class Base < BaseService
    include UserValidations

    attr_reader :current_user, :item_type, :item_id

    def initialize(current_user, item_type, item_id)
      @current_user = current_user
      @item_type, @item_id = item_type, item_id
    end

    def create
      case item_type
      when 'category'
        Bookmarks::Categories::Create.new(current_user, item_id).call
      when 'city'
        Bookmarks::Cities::Create.new(current_user, item_id).call
      else
        ForbiddenError.new(:item_type, 'invalid item_type')
      end
    end

    def destroy
      case item_type
      when 'category'
        Bookmarks::Categories::Destroy.new(current_user, item_id).call
      when 'city'
        Bookmarks::Cities::Destroy.new(current_user, item_id).call
      else
        ForbiddenError.new(:item_type, 'invalid item_type')
      end
    end
  end
end
