class CreateScores < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
    create_table :moneys, id: :uuid do |t|
      t.uuid :user_id, index: true, null: false
      t.decimal :amount,  index: true, null: false, default: 0
      t.string  :type, null: false, default: 'RealMoney'

      t.timestamps
    end
    add_index :moneys, :created_at
    add_index :moneys, :updated_at
    # add_index :scores, [:user_id, :type], unique: true не работает в sqlite
  end
end
