class Api::V1::ReviewsController < ApplicationController
  before_action :authenticate_user_from_token!
  before_action :get_review, only: [:update, :destroy]

  def create
    result = Reviews::Create.new(@current_user, review_params).call
    render_response(result)
  end

  def update
    prm = review_params
    prm.extract!(:task_id)
    result = Reviews::Update.new(@current_user, @review, prm).call
    render_response(result)
  end

  def destroy
    result = Reviews::Destroy.new(@current_user, @review).call
    render_response(result)
  end

  private

  def get_review
    @review = Review.find_by(id: params[:id])
  end

  def review_params
    params.require(:review).permit(:task_id, :description, :rating)
  end
end