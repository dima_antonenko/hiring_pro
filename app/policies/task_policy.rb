class TaskPolicy < ApplicationPolicy

  attr_accessor(:user, :task)

  def initialize(user, task = nil)
    @user, @task = user, task
  end


  def create?
    user_valid?(user)
    # @todo написать ограничение на количество задач в день
  end
end