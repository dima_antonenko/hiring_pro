module Attachments
  class Base < BaseService
    include UserValidations
    include TaskValidations
    include ImageValidations

    attr_reader :current_user, :item_type, :item_id, :attachments, :attachment_id

    def initialize(current_user, item_type, item_id, attachments:, attachment_id:)
      @current_user, @attachments = current_user, attachments
      @item_type, @item_id = item_type, item_id
      @attachment_id = attachment_id
    end

    def create
      case item_type
      when 'task'
        Attachments::Tasks::Create.new(current_user, item_id, attachments).call
      else
        ForbiddenError.new('invalid item_type')
      end
    end


    def destroy
      case item_type
      when 'task'
        Attachments::Tasks::Destroy.new(current_user, item_id, attachment_id).call
      else
        ForbiddenError.new('invalid item_type')
      end
    end
  end
end
