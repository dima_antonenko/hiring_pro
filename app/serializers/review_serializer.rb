class ReviewSerializer
  include FastJsonapi::ObjectSerializer

  attributes :description, :rating, :state, :deleted

  belongs_to :task
  # belongs_to :user, base_key: "recipient_id" # @todo разобраться как передать кастомный внешний ключ

  attribute :recipient_data do |review|
    u = review.user
    #u = User.find_by(id: review.recipient_id)
    {id: u.id, email: u.email, total_rating: u.total_rating, rating: u.rating, reviews_qty: u.reviews_qty}
  end
end