class Admin::TasksController < AdminController
  before_action :get_task, only: [:edit]

  def index
    @tasks = Task.all.includes(:user, :city, :category)
  end

  def edit; end

  private
  
  def get_task
    @task = Task.find_by(id: params[:id])
  end
end