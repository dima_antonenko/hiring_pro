class TicketSerializer
  include FastJsonapi::ObjectSerializer
  attributes :subject, :description, :reason_type, :state, :admin_answered, :created_at, :deleted_at

  belongs_to :user
  has_many   :ticket_messages
end