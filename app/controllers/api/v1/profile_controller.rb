class Api::V1::ProfileController < ApplicationController
  before_action :authenticate_user_from_token!, only: [:update, :score]

  def update
    result = Profile::Update.new(current_user, user_params).call
    render_response(result)
  end

  def content_limits
    result = Profile::ContentLimits.new.call
    render_json(limits: result)
  end

  private

  def user_params
    params.require(:user).permit(:name)
  end
end