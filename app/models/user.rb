class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  before_save  :ensure_authentication_token
  after_create :create_moneys

  has_one :real_money
  has_one :virtual_money

  has_many     :money_logs
  has_many     :tasks
  has_many     :task_requests
  has_many     :reviews, foreign_key: "recipient_id"
  has_many     :tickets

  belongs_to   :city

  has_one_attached :avatar

  def avatar_full_path
    ActiveStorage::Blob.service.path_for(self.avatar.key)
  end

  private

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def create_moneys
    RealMoney.create(user_id: self.id)
    VirtualMoney.create(user_id: self.id)
  end
end
