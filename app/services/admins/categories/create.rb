module Admins
  module Categories
    class Create < BaseService
      attr_reader :params, :category, :parent_category

      def initialize(params, parent_category_id)
        @params = params
        @parent_category = Category.find_by(id: parent_category_id)
        @category = Category.new
      end

      def call
        @category.assign_attributes(params)
        save_and_return(@category) do
          @category.move_to_child_of(@parent_category) if @parent_category
        end
      end
    end
  end
end
