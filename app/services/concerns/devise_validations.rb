module DeviseValidations
  def user_exist?(email)
    user = User.find_by(email: email)
    raise ForbiddenError.new(:user_id, 'User already exist') if user
  end

  def accept_terms?(accept_terms = '1')
    raise ForbiddenError.new(:accept_terms, 'You not accept terms ') if accept_terms == '0' || !accept_terms
  end
end