class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets, id: :uuid do |t|
      t.uuid    :user_id, index: true, null: false
      t.string  :subject, null: false
      t.text    :description
      t.integer :reason_type, index: true, null: false
      t.integer :state, index: true, null: false, default: 0
      t.boolean :admin_answered, index: true, null: false, default: false
      t.datetime :deleted_at, index: true
      t.timestamps
    end
    add_index :tickets, :created_at
    add_index :tickets, :updated_at
  end
end
