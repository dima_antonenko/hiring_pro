class Api::V1::TicketMessagesController < ApplicationController
  before_action :authenticate_user_from_token!

  def create
    result = TicketMessages::Create.new(current_user, ticket_message_params).call
    render_response(result)
  end

  private

  def ticket_message_params
    params.require(:ticket_message).permit(:ticket_id, :content)
  end
end