module CityValidations
  def city_exist?(city)
    raise PermissionError.new(:city_id, 'City not exist') unless city
  end

  def city_not_deleted?(city)
    raise PermissionError.new(:city_id, 'City must be not deleted') if city.deleted
  end
end
