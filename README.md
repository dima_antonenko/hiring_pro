# README

cap production puma:restart

RAILS_ENV=production rbenv exec bundle exec rake db:seed



Название в латинской транскрипции: zambian.pro

Целевая аудитория: Мастера, выполняющие услуги по найму и клиенты, которые желают воспользоваться этими услугами в стране Замбия.

Форма иконки/логотипа: квадрат

Цветовое решение, количество цветов: не более трех

Предполагаемые коммуникации использования логотипа: Сайт и мобильные приложения.

Основные требования к иконке и логотипу
— знак должен быть самодостаточным, т.е. должна быть возможность использовать его без графического написания,
-логотип должен легко прочитывается и запоминаться,

— логотип должен вызывать ассоциации с деловым обедом,

— не должен содержать множество мелких деталей и тонкие линии


Какие логотипы из существующих аналогов нравятся:
https://www.iphones.ru/wp-content/uploads/2014/05/main2.jpg
https://prostomac.com/wp-content/uploads/2015/02/Profi_0.jpg
https://image.shutterstock.com/image-vector/job-search-logo-260nw-335172137.jpg
https://thumbs.dreamstime.com/b/job-vacancy-work-search-logo-design-template-job-vacancy-work-search-logo-design-113122017.jpg
https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRFXQvBL4IXy9e8_e5YP0HgDeg9TTRWqg90OQ&usqp=CAU
https://i.pinimg.com/originals/bf/6f/25/bf6f25556f90a4f994c532350cdbb511.png


RAILS_ENV=production DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rbenv exec bundle exec rake db:seed
