class Api::V1::UsersController < ApplicationController
  before_action :authenticate_user_from_token!, only: [:me]
  
  def me
    render json: CurrentUserSerializer.new(current_user)

    # render_response current_user, "CurrentUserSerializer"
  end

  def show
    user = User.find(params[:id])
    render_item(user, 'UserSerializer', {}, params[:include].to_s.split(","))
  end
end