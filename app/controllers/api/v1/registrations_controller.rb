class Api::V1::RegistrationsController < Devise::RegistrationsController

  def create
    result = Devise::SignUp.new(sign_up_params).call
    render_response(result)
  end

  private

  def sign_up_params
    params.require(:registration).permit(:email, :password, :password_confirmation, :name, :phone, :city_id, :accept_terms)
  end

  def account_update_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :phone, :city_id)
  end
end