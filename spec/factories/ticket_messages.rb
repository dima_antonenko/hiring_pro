FactoryBot.define do
  factory :ticket_message do
    content { Faker::Lorem.sentence(word_count: 10) }
    ticket
  end
end
