@approved_task = Task.find_by(title: 'Task with approved requests')
@task_creator = @approved_task.user
@request_creator = User.find_by(email: 'request_creator@test.ru')
@task_request = TaskRequest.where(task_id: @approved_task.id, user_id: @request_creator.id).first


result = TaskRequests::Approve.new(@task_creator, @task_request).call
puts "Approved task_request: #{result.attributes} for task: #{@approved_task.attributes} \n \n"