class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :title, index: true, null: false
      t.integer :default_duration_days, index: true, null: false, default: 30

      t.string :system_name, index: true, null: false
      t.integer :position, index: true, null: false, default: 0

      t.integer :parent_id, null: true, index: true
      t.integer :lft, null: false, index: true
      t.integer :rgt, null: false, index: true

      # optional fields
      t.integer :depth, :null => false, :default => 0
      t.integer :children_count, :null => false, :default => 0

      t.boolean :deleted, index: true, null: false, default: false

      t.timestamps
    end
  end
end
