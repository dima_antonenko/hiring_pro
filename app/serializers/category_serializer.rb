class CategorySerializer
  include FastJsonapi::ObjectSerializer

  attributes :title, :parent_id, :system_name
  has_many :categories, if: Proc.new { |record| record.parent_id.nil? } # optimize, don't select child categories from non-parent categories
end