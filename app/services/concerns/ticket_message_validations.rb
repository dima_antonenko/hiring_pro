module TicketMessageValidations
  MAX_TICKET_MESSAGE_TODAY = 100

  def user_ticket_messages_limits_valid?(user)
    raise PermissionError.new(:user_id, 'user ticket_messages limits not exceeded') if messages_today(user.id) > MAX_TICKET_MESSAGE_TODAY
  end

  private

  def messages_today(user_id)
    TicketMessage.where(user_id: user_id, created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).size
  end
end