const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const datatable =  require('./loaders/datatable')

environment.plugins.prepend('Provide',
	new webpack.ProvidePlugin({
		$: 'jquery/src/jquery',
		jQuery: 'jquery/src/jquery',
		jquery: 'jquery',
		'window.jQuery': 'jquery',
	})
)

environment.loaders.get('sass').use.splice(-1, 0, {
	loader: 'resolve-url-loader'
})

environment.loaders.append('expose', {
	test: require.resolve('jquery'),
	use: [{
		loader: 'expose-loader',
		options: '$'
	}]
})

environment.loaders.prepend('coffee', datatable)

module.exports = environment

// ../../assets/fonts/inter-ui/Inter-UI-Black.woff2