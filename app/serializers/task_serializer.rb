class TaskSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :service_location, :price, :payment_type, :start_date,
             :completion_date, :state, :deleted, :completed_at

  has_many :task_requests, if: Proc.new { |task, params| params && params[:current_user].try(:id) == task.user_id }
  has_many :reviews

  belongs_to :city
  belongs_to :category
  belongs_to :user

  belongs_to :task_request, if: Proc.new { |task, params| !params.try(:[], :current_user).blank? } do |task, params|
    task.task_requests.where(user_id: params[:current_user].id).last
  end

  attribute :avatar_full_path do |task|
    if task.avatar.attached?
      Rails.application.routes.url_helpers.rails_blob_path(task.avatar, only_path: true)
    else
      '/static_data/noimage/noimage.png'
    end
  end

  attribute :attachments do |task|
    if task.attachments.attached?
      task.attachments.map { |a| { id: a.id,
                                   path: Rails.application.routes.url_helpers.rails_blob_path(a, only_path: true) }}
    else
      []
    end
  end

  attribute :responded_for_current_user do |task, params|
    if params[:current_user]
      task.task_requests.where(user_id: params[:current_user].id, state: :active).exists?
    else
      false
    end
  end

  # attribute :categories do |task|
  #   category = task.category
  #   parent_category = category.parent
  #   {category_id: category.id,
  #    category_title: category.title,
  #    parent_category_id: parent_category.id,
  #    parent_category_title: parent_category.title}
  # end
end
