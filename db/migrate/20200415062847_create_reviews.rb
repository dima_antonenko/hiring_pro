class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews, id: :uuid do |t|
      t.uuid :sender_id, index: true, null: false
      t.uuid :recipient_id, index: true, null: false
      t.uuid :task_id, index: true, null: false

      t.text :description, index: true, null: false
      t.integer :rating, index: true, null: false
      t.integer :state, index: true, null: false, default: 0
      t.boolean :deleted, index: true, null: false, default: false
      t.timestamps
    end
  end
end
