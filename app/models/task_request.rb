class TaskRequest < ApplicationRecord
  belongs_to :user
  belongs_to :task

  enum state: { active: 0, approved: 1, rejected: 2, completed: 3 }

  validates :message, presence: true, length: {maximum: 400}
end
