class Ticket < ApplicationRecord
  has_many   :ticket_messages
  belongs_to :user

  scope :visible, -> { where.not(state: :deleted) }

  enum reason_type:  { finance: 0, technical: 1, fraud: 2 }
  enum state:  { active: 0, answered: 1, deleted: 2  }

  validates :subject, presence: true
end
