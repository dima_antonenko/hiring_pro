module Attachments
  module Tasks
    class Destroy < Attachments::Base
      attr_reader :current_user, :task, :attachment

      def initialize(current_user, task_id, attachment_id)
        @current_user, @task = current_user, Task.find_by(id: task_id)
        @attachment = @task&.attachments&.find_by(id: attachment_id)
      end

      def call
        validate!
        attachment.purge
        save_and_return(task)
      end

      private

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        user_active?(current_user)
        task_not_deleted?(task)
        task_has_valid_completion?(task)
        user_has_access?(current_user, task)
        task_not_approved?(task)
        image_exist?(attachment)
      end
    end
  end
end
