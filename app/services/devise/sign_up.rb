module Devise
  class SignUp < BaseService
    include DeviseValidations

    attr_reader :email, :params, :user, :accept_terms

    def initialize(params)
      @email = params[:email]
      @params = params
      @user = User.new
      @accept_terms = params[:accept_terms]
    end

    def call
      validate!
      params.extract!(:accept_terms)
      user.assign_attributes(params)
      save_and_return(user)
    end

    private

    def validate!
      user_exist?(email)
      accept_terms?(params[:accept_terms])
    end
  end
end