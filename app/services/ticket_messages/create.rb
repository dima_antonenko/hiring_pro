module TicketMessages
  class Create < BaseService
    include UserValidations
    include TicketValidations
    include TicketMessageValidations

    attr_accessor :current_user, :ticket_message, :params, :ticket

    def initialize(current_user, params)
      @current_user = current_user
      @params = params
      @ticket_message = TicketMessage.new
      @ticket = Ticket.find_by(id: params[:ticket_id])
    end

    def call
      validate!
      add
      save_and_return(ticket_message) do
        ticket.update_attribute(:admin_answered, false)
      end
    end

    def add
      ticket_message.user_id = current_user.id
      ticket_message.assign_attributes(params)
    end

    private

    def validate!
      ticket = Ticket.find_by(id: params[:ticket_id])
      user_active?(current_user)
      ticket_active?(ticket)
      user_ticket_messages_limits_valid?(current_user)
    end
  end
end