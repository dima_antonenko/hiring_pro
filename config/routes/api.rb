Rails.application.routes.draw do
  constraints format: :json do
    devise_for :users, path: "api/v1/users", controllers: {registrations: 'api/v1/registrations', sessions: "api/v1/sessions", passwords: "api/v1/passwords"}
    namespace :api, defaults: {format: 'json'} do
      namespace :v1 do
        resource :account, only: :update

        resources :cities, only: :index
        resources :categories, only: :index
        resources :cities, only: :index

        resources :tasks, except: [:new, :edit, :index] do
          post 'complete', on: :member
        end
        get '/tasks',    to: "tasks#index", constraints: lambda { |request| !request.query_parameters['my'].present? }
        get '/tasks',    to: "tasks#my",    constraints: lambda { |request| request.query_parameters['my'].present? }
        get '/newsfeed', to: "tasks#newsfeed"

        resources :task_requests, only: [:create, :update, :destroy, :index] do
          post 'approve', on: :member
        end

        resources :reviews,       only: [:create, :update, :destroy]
        resources :avatars,       only: [:create, :update, :destroy]
        resources :attachments,   only: [:create, :destroy]
        resources :bookmarks,     only: [:create, :destroy]
        resources :users,         only: [:show] do
          collection do
            get :me
          end
        end

        # support
        resources :tickets,         only: [:index, :create, :update, :destroy, :show]
        resources :ticket_messages, only: [:create]
      end
    end
  end


end
