source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.4'

# architecture
gem 'rails', '~> 6.0.2', '>= 6.0.2.2'
gem 'devise'
gem 'awesome_nested_set'
gem 'will_paginate', '~> 3.1.0'
gem 'fast_jsonapi'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'puma', '~> 4.1'
gem 'rack-cors', require: 'rack/cors'
gem 'pg'
gem 'pg_search', '~> 2.3', '>= 2.3.2'

# other
gem 'faker'

# assets
gem "aws-sdk-s3", require: false
gem 'webpacker', '~> 4.0'
gem "image_processing"

# documentation
gem 'rswag-api'
gem 'rswag-ui'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'meta_request'
  gem 'bullet'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem "letter_opener"
end

group :development, :test do
  gem 'sqlite3'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'hirb'
  gem 'hirb-unicode'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  gem 'rspec-rails', '~> 3.8'
  gem 'factory_bot_rails'
  gem 'database_cleaner'
  gem 'rswag-specs'

  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-rvm'
  gem 'capistrano3-puma'
end

gem 'mimemagic', github: 'mimemagicrb/mimemagic', ref: '01f92d86d15d85cfd0f20dabd025dcbd36a8a60f'