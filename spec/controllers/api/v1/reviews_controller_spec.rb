require 'rails_helper'
RSpec.describe Api::V1::ReviewsController, type: :request do
  let! (:task_creator) { create(:user) }
  let! (:request_creator) { create(:user) }

  let! (:task) { create(:task, user_id: task_creator.id, state: :completed) }
  let! (:task_request) { create(:task_request, task_id: task.id, state: :completed, user_id: request_creator.id) }

  def create_review(user, extra_params = nil)
    review_params = attributes_for(:review).merge({task_id: task.id})
    review_params.merge!(extra_params) if extra_params
    post api_v1_reviews_path, params: {review: review_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_review_id
    json = create_review(request_creator)
    json['data']['id']
  end

  def update_review(user, review_id, extra_params)
    review_params = attributes_for(:review)
    review_params.merge!(extra_params) if extra_params
    patch api_v1_review_path(review_id), params: {review: review_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def destroy_review(user, review_id)
    delete api_v1_review_path(review_id), headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # create review
  describe 'POST /api/v1/reviews' do
    context 'positive scenarios' do
      it 'task request creator can create review' do
        json = create_review(request_creator)
        expect(json['data']['attributes']['description']).to be_truthy
      end

      it 'task creator rating will be changed after adding review' do
        json = create_review(request_creator)
        expect(json['data']['attributes']['recipient_data']['total_rating']).to eq(5)
      end
    end

    context 'negative scenarios' do
      it 'task creator cannot add review to myself' do
        create_review(task_creator)
        expect(response).to have_http_status(422)
      end

      it 'user can`t add review to not completed task' do
        task.active!
        create_review(task_creator)
        expect(response).to have_http_status(422)
      end

      it 'user can`t add review to not completed task_request' do
        task_request.active!
        create_review(task_creator)
        expect(response).to have_http_status(422)
      end

      it 'user can`t add review to invalid task' do
        create_review(task_creator, {task_id: 0})
        expect(response).to have_http_status(422)
      end

      it 'user can`t add review to old task' do
        task.update_attribute(:completed_at, 1.month.ago)
        create_review(task_creator)
        expect(response).to have_http_status(422)
      end

      it 'user can`t add two reviews' do
        create_review(task_creator)
        create_review(task_creator)
        expect(response).to have_http_status(422)
      end
    end
  end

  # update review
  describe 'PATCH /api/v1/reviews/:id' do
    context 'positive scenarios' do
      it 'review creator can update his review' do
        json = update_review(request_creator, get_review_id, {description: 'new_description', rating: 1})
        expect(json['data']['attributes']['description']).to eq('new_description')
      end

      it 'task creator has valid rating after review' do
        json = update_review(request_creator, get_review_id, {description: 'new_description', rating: 1})
        expect(json['data']['attributes']['recipient_data']['rating'].to_i).to eq(1)
      end
    end

    context 'negative scenarious' do
      it 'user cannot update deleted review' do
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:deleted, true)
        json = update_review(request_creator, review_id, {description: 'new_description', rating: 1})
        expect(response).to have_http_status(422)
      end

      it 'user cannot update not his review' do
        other_user = create(:user)
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:sender_id, other_user.id)
        update_review(request_creator, review_id, {description: 'new_description', rating: 1})
        expect(response).to have_http_status(422)
      end

      it 'user cannot update old review' do
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:created_at, 1.day.ago)
        update_review(request_creator, review_id, {description: 'new_description', rating: 1})
        expect(response).to have_http_status(422)
      end
    end

    # @todo возможно нужно переписать, но на всякий случай пусть будут
    context 'check rating calculations' do
      it 'create first review' do
        json = create_review(request_creator, {rating: 5})
        expect(json['data']['attributes']['recipient_data']['total_rating'].to_i).to eq(5)
        expect(json['data']['attributes']['recipient_data']['rating'].to_i).to eq(5)
        expect(json['data']['attributes']['recipient_data']['reviews_qty']).to eq(1)
      end

      it 'create second review' do
        task_creator.update({total_rating: 5, rating: 5, reviews_qty: 1})
        task_creator.save
        json = create_review(request_creator, {rating: 5})
        expect(json['data']['attributes']['recipient_data']['total_rating'].to_i).to eq(10)
        expect(json['data']['attributes']['recipient_data']['rating'].to_i).to eq(5)
        expect(json['data']['attributes']['recipient_data']['reviews_qty']).to eq(2)
      end

      it 'create and update review' do # 5 + 5(1)
        task_creator.update({total_rating: 5, rating: 5, reviews_qty: 1})
        task_creator.save
        json = create_review(request_creator, {rating: 5})
        id = json['data']['id']
        json = update_review(request_creator, id, {description: 'new_description', rating: 1})
        expect(json['data']['attributes']['recipient_data']['total_rating'].to_i).to eq(6)
        expect(json['data']['attributes']['recipient_data']['rating'].to_i).to eq(3)
        expect(json['data']['attributes']['recipient_data']['reviews_qty']).to eq(2)
      end

      it 'create and destroy' do
        task_creator.update({total_rating: 5, rating: 5, reviews_qty: 1})
        task_creator.save
        json = create_review(request_creator, {rating: 5})
        review_id = json['data']['id']
        destroy_review(request_creator, review_id)
      end
    end
  end

  # delete review
  describe 'DELETE /api/v1/reviews/:id' do
    context 'positive scenarios' do
      it 'review creator can destroy his review' do
        json = destroy_review(request_creator, get_review_id)
        expect(json['data']['attributes']['deleted']).to be_truthy
      end

      it 'task creator has valid rating after destroy review' do
        json = destroy_review(request_creator, get_review_id)
        expect(json['data']['attributes']['recipient_data']['total_rating']).to eq(0)
      end
    end

    context 'negative scenarious' do
      it 'user cannot destroy deleted review' do
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:deleted, true)
        json = destroy_review(request_creator, review_id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot update not his review' do
        other_user = create(:user)
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:sender_id, other_user.id)
        destroy_review(request_creator, review_id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot update old review' do
        review_id = get_review_id
        Review.find_by(id: review_id).update_attribute(:created_at, 1.day.ago)
        destroy_review(request_creator, review_id)
        expect(response).to have_http_status(422)
      end
    end
  end
end
