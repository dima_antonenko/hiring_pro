module ReviewValidations
  MAX_UPDATE_TIME = 5.minutes

  def review_already_exist?(user, task)
    reviews_qty = Review.where(sender_id: user.id, task_id: task.id).size
    raise PermissionError.new(:task_id, 'You already set review to this task') if reviews_qty > 0
  end

  def review_has_valid_state?(review)
    raise PermissionError.new(:state,'This review has invalid state') if !review || review.deleted
  end

  def user_related_with_review?(user, review)
    raise PermissionError.new(:sender_id,'User not related for this review') if review.sender_id != user.id
  end

  def review_has_valid_time?(review)
    raise PermissionError.new(:created_at,'This review has invalid state') if review.created_at < MAX_UPDATE_TIME.ago
  end
end