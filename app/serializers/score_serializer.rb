class ScoreSerializer
  include FastJsonapi::ObjectSerializer

  attributes :real, :virtual

  belongs_to :user


  attribute :real_operations do |score|
    score.score_logs.where(real_score: true)
  end

  attribute :virtual_operations do |score|
    score.score_logs.where(real_score: false)
  end
end
