class Admin::TaskRequestsController < AdminController
  before_action :get_request, only: [:edit, :update, :destroy]

  def index
    @requests = TaskRequest.all
  end

  def edit; end

  def update
    @request.assign_attributes(task_request_params)
    @request.save ? flash[:success] = "Request will be updated" : flash[:errors] = @request.errors.full_messages
    redirect_back(fallback_location: admin_task_requests_url)
  end

  def destroy
    @request.update_attribute(:deleted, true)
    redirect_back(fallback_location: admin_task_requests_url)
  end

  private

  def task_request_params
    params.require(:task_request).permit(:price, :message)
  end

  def get_request
    @request = TaskRequest.find(params[:id])
  end
end