class Admin::TicketMessagesController < AdminController
  def create
    message = TicketMessage.create(ticket_message_params.merge({admin_id: current_admin.id}))
    Ticket.find_by(id: ticket_message_params[:ticket_id]).update_attribute(:admin_answered, true)
    redirect_back(fallback_location: '/admin')
  end

  private

  def ticket_message_params
    params.require(:ticket_message).permit(:ticket_id, :content)
  end
end