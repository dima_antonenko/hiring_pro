require 'rails_helper'

RSpec.describe Api::V1::CategoriesController, type: :request do

  let! (:children_category) {create(:category, title: 'Дочерняя категория')}
  let! (:parent_category) { children_category.parent }

  def get_categories
    get api_v1_categories_path
    JSON.parse(response.body)
  end

  # get all categories
  describe 'GET /api/v1/categories' do
    it 'User can get all categories | response has valid http status' do
      json = get_categories
      expect(response).to have_http_status(200)
    end
  end
end
