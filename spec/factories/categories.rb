FactoryBot.define do

  # по умолчанию создается дочерняя категория
  # доступ к родительской(root) category.parent
  factory :category do
    title { Faker::Lorem.sentence(word_count: 2) }
    system_name { Faker::Lorem.sentence(word_count: 1) }
    default_duration_days { 30 }

    after(:create) do |children_category|
      parent_category = Category.create!(title: 'Parent Category', system_name: '')
      children_category.move_to_child_of(parent_category)
    end
  end
end
