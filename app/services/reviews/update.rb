module Reviews
  class Update < BaseService
    include ReviewValidations
    include UserValidations

    attr_reader :current_user, :params, :review

    def initialize(current_user, review, params)
      @current_user, @review, @params = current_user, review, params
    end

    def call
      validate!
      old_rating = review.rating
      review.assign_attributes(params)
      save_and_return(review) do
        increase_rating(review.user, review.rating, old_rating)
      end
    end

    private

    def validate!
      user_active?(current_user)
      review_has_valid_state?(review)
      user_related_with_review?(current_user, review)
      review_has_valid_time?(review)
    end
  end
end