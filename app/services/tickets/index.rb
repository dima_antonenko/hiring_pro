module Tickets
  class Index < BaseService
    include UserValidations

    attr_accessor :user, :params

    def initialize(user, params)
      @user, @params = user, params
    end

    def call
      validate!
      Ticket.visible.where(user_id: user.id)
    end

    private

    def validate!
      user_active?(user)
    end
  end
end