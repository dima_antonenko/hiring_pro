# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_08_085833) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
    t.index ["updated_at"], name: "index_active_storage_attachments_on_updated_at"
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
    t.index ["updated_at"], name: "index_active_storage_blobs_on_updated_at"
  end

  create_table "admin_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["name"], name: "index_admin_users_on_name"
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "title", null: false
    t.integer "default_duration_days", default: 30, null: false
    t.string "system_name", null: false
    t.integer "position", default: 0, null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
    t.integer "depth", default: 0, null: false
    t.integer "children_count", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["default_duration_days"], name: "index_categories_on_default_duration_days"
    t.index ["deleted"], name: "index_categories_on_deleted"
    t.index ["lft"], name: "index_categories_on_lft"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
    t.index ["position"], name: "index_categories_on_position"
    t.index ["rgt"], name: "index_categories_on_rgt"
    t.index ["system_name"], name: "index_categories_on_system_name"
    t.index ["title"], name: "index_categories_on_title"
  end

  create_table "cities", force: :cascade do |t|
    t.string "title"
    t.integer "position", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.string "meta_title"
    t.text "meta_description"
    t.text "meta_keywords"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["created_at"], name: "index_cities_on_created_at"
    t.index ["deleted"], name: "index_cities_on_deleted"
    t.index ["meta_description"], name: "index_cities_on_meta_description"
    t.index ["meta_keywords"], name: "index_cities_on_meta_keywords"
    t.index ["meta_title"], name: "index_cities_on_meta_title"
    t.index ["position"], name: "index_cities_on_position"
    t.index ["title"], name: "index_cities_on_title"
    t.index ["updated_at"], name: "index_cities_on_updated_at"
  end

  create_table "money_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "money_id"
    t.decimal "amount_before", default: "0.0", null: false
    t.decimal "amount_after", default: "0.0", null: false
    t.decimal "amount_diff", default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "commit"
    t.index ["created_at"], name: "index_money_logs_on_created_at"
    t.index ["money_id"], name: "index_money_logs_on_money_id"
    t.index ["updated_at"], name: "index_money_logs_on_updated_at"
  end

  create_table "moneys", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.decimal "amount", default: "0.0", null: false
    t.string "type", default: "RealMoney", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["amount"], name: "index_moneys_on_amount"
    t.index ["created_at"], name: "index_moneys_on_created_at"
    t.index ["updated_at"], name: "index_moneys_on_updated_at"
    t.index ["user_id"], name: "index_moneys_on_user_id"
  end

  create_table "reviews", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "sender_id", null: false
    t.uuid "recipient_id", null: false
    t.uuid "task_id", null: false
    t.text "description", null: false
    t.integer "rating", null: false
    t.integer "state", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted"], name: "index_reviews_on_deleted"
    t.index ["description"], name: "index_reviews_on_description"
    t.index ["rating"], name: "index_reviews_on_rating"
    t.index ["recipient_id"], name: "index_reviews_on_recipient_id"
    t.index ["sender_id"], name: "index_reviews_on_sender_id"
    t.index ["state"], name: "index_reviews_on_state"
    t.index ["task_id"], name: "index_reviews_on_task_id"
  end

  create_table "task_requests", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "task_id", null: false
    t.uuid "user_id", null: false
    t.integer "state", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.integer "price", null: false
    t.string "message"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted"], name: "index_task_requests_on_deleted"
    t.index ["price"], name: "index_task_requests_on_price"
    t.index ["state"], name: "index_task_requests_on_state"
    t.index ["task_id"], name: "index_task_requests_on_task_id"
    t.index ["user_id"], name: "index_task_requests_on_user_id"
  end

  create_table "tasks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", null: false
    t.string "description"
    t.string "service_location"
    t.integer "price", null: false
    t.datetime "start_date"
    t.datetime "completion_date", null: false
    t.integer "payment_type", null: false
    t.integer "state", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.integer "category_id", null: false
    t.uuid "user_id", null: false
    t.integer "city_id", null: false
    t.datetime "completed_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_tasks_on_category_id"
    t.index ["city_id"], name: "index_tasks_on_city_id"
    t.index ["completed_at"], name: "index_tasks_on_completed_at"
    t.index ["deleted"], name: "index_tasks_on_deleted"
    t.index ["payment_type"], name: "index_tasks_on_payment_type"
    t.index ["state"], name: "index_tasks_on_state"
    t.index ["user_id"], name: "index_tasks_on_user_id"
  end

  create_table "ticket_messages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "admin_id"
    t.uuid "ticket_id", null: false
    t.text "content", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_id"], name: "index_ticket_messages_on_admin_id"
    t.index ["created_at"], name: "index_ticket_messages_on_created_at"
    t.index ["ticket_id"], name: "index_ticket_messages_on_ticket_id"
    t.index ["updated_at"], name: "index_ticket_messages_on_updated_at"
    t.index ["user_id"], name: "index_ticket_messages_on_user_id"
  end

  create_table "tickets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.string "subject", null: false
    t.text "description"
    t.integer "reason_type", null: false
    t.integer "state", default: 0, null: false
    t.boolean "admin_answered", default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin_answered"], name: "index_tickets_on_admin_answered"
    t.index ["created_at"], name: "index_tickets_on_created_at"
    t.index ["deleted_at"], name: "index_tickets_on_deleted_at"
    t.index ["reason_type"], name: "index_tickets_on_reason_type"
    t.index ["state"], name: "index_tickets_on_state"
    t.index ["updated_at"], name: "index_tickets_on_updated_at"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "authentication_token"
    t.boolean "deleted", default: false, null: false
    t.integer "city_id", null: false
    t.string "phone", null: false
    t.integer "reviews_qty", default: 0, null: false
    t.decimal "rating", default: "0.0", null: false
    t.integer "total_rating", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "bookmark_category_ids", default: [], null: false, array: true
    t.integer "bookmark_city_ids", default: [], null: false, array: true
    t.index ["authentication_token"], name: "index_users_on_authentication_token"
    t.index ["city_id"], name: "index_users_on_city_id"
    t.index ["created_at"], name: "index_users_on_created_at"
    t.index ["deleted"], name: "index_users_on_deleted"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["phone"], name: "index_users_on_phone"
    t.index ["rating"], name: "index_users_on_rating"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["reviews_qty"], name: "index_users_on_reviews_qty"
    t.index ["total_rating"], name: "index_users_on_total_rating"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
