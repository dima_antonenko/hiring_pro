city = City.find_by(title: 'Lusaka')

User.destroy_all
User.create!(email: "task_creator@test.ru",          password: "12345678", password_confirmation: "12345678",
             phone: Faker::PhoneNumber.phone_number, city: city, name: 'Alex')
User.create!(email: "request_creator@test.ru",       password: "12345678", password_confirmation: "12345678",
             phone: Faker::PhoneNumber.phone_number, city: city, name: 'Oleg')
User.create!(email: "other_request_creator@test.ru", password: "12345678", password_confirmation: "12345678",
             phone: Faker::PhoneNumber.phone_number, city: city, name: 'Igor')
User.create!(email: "fictive_admin@test.ru",         password: "12345678", password_confirmation: "12345678",
             phone: Faker::PhoneNumber.phone_number, city: city, name: 'Valentin')

puts "Users added \n \n"