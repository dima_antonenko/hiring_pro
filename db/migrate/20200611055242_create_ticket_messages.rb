class CreateTicketMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :ticket_messages, id: :uuid do |t|
      t.uuid  :user_id, index: true
      t.uuid  :admin_id, index: true
      t.uuid  :ticket_id, index: true, null: false
      t.text     :content,  null: false
      t.timestamps
    end
    add_index :ticket_messages, :created_at
    add_index :ticket_messages, :updated_at
  end
end
