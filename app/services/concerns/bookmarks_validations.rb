module BookmarksValidations
  def category_not_bookmarked?(user, category_id)
    raise PermissionError.new(:category_id, 'Category already in bookmarks') if user.bookmark_category_ids.include?(category_id)
  end

  def category_in_bookmarks?(user, category_id)
    raise PermissionError.new(:category_id, 'Category not in bookmarks') unless user.bookmark_category_ids.include?(category_id)
  end

  def city_not_bookmarked?(user, city_id)
    raise PermissionError.new(:city_id, 'City already in bookmarks') if user.bookmark_city_ids.include?(city_id)
  end

  def city_in_bookmarks?(user, city_id)
    raise PermissionError.new(:city_id, 'City not in bookmarks') unless user.bookmark_city_ids.include?(city_id)
  end
end
