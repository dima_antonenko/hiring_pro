class CreateTaskRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :task_requests, id: :uuid do |t|
      t.uuid    :task_id, index: true, null: false
      t.uuid    :user_id, index: true, null: false

      t.integer :state, index: true, null: false, default: 0
      t.boolean :deleted, index: true, null: false, default: false

      t.integer :price, index: true, null: false

      t.string :message
      t.timestamps
    end
  end
end
