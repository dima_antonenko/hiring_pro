class Admin::TicketsController < AdminController
  before_action :get_ticket, only: [:show, :destroy]

  def index
    @active_tickets = Ticket.active.includes(:ticket_messages)
    @deteled_tickets = Ticket.deleted.includes(:ticket_messages)
  end

  def show
    @message = TicketMessage.new
    @messages = TicketMessage.where(ticket_id: @ticket.id)
  end

  def destroy
    @ticket.deleted!
    @ticket.deleted_at = DateTime.now
    @ticket.save
    redirect_back(fallback_location: '/admin')
  end

  private

  def get_ticket
    @ticket = Ticket.find(params[:id])
  end
end