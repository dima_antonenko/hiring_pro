require.context('../themes/admin/assets/img', true);

import 'datatables.net-dt/css/jquery.dataTables.css'
import 'datatables.net-select-dt/css/select.dataTables.css'
require('../themes/admin/lib/@fortawesome/fontawesome-free/css/all.css');
require('../themes/admin/lib/ionicons/css/ionicons.min.css');
require('../themes/admin/lib/typicons.font/typicons.css');
require('../themes/admin/lib/prismjs/themes/prism-vs.css');
require('../themes/admin/lib/select2/css/select2.min.css');
require('../themes/admin/assets/css/dashforge.css');
require('../themes/admin/assets/css/dashforge.demo.css');
require('../themes/admin/assets/css/dashforge.chat.css');
require('../themes/admin/assets/css/custom.scss');


require("@rails/ujs").start()
require("jquery");

import $ from 'jquery';
global.$ = jQuery;

require('../themes/admin/lib/bootstrap/js/bootstrap.bundle.min.js');
require('../themes/admin/lib/feather-icons/feather.min.js');
require('imports-loader?define=>false!datatables.net')(window, $)
require('imports-loader?define=>false!datatables.net-select')(window, $)
require('../themes/admin/lib/cleave.js/cleave.min.js');
require('../themes/admin/lib/select2/js/select2.min.js');
require('../themes/admin/assets/js/dashforge.js');


$(function(){
	'use strict'

	$('#smart-table').DataTable({
		language: {
			searchPlaceholder: 'Search...',
			sSearch: '',
			lengthMenu: '_MENU_ items/page',
		}
	});

	$('#smart-table2').DataTable({
		language: {
			searchPlaceholder: 'Search...',
			sSearch: '',
			lengthMenu: '_MENU_ items/page',
		}
	});
});
