task_creator = User.find_by(email: 'task_creator@test.ru')
category = Category.find_by(title: 'Courier service')

task_params = {title: 'Completed task',
               description: 'Description for first task',
               price: 100,
               start_date: 1.day.from_now,
               completion_date: 10.days.from_now,
               payment_type: 0,
               category_id: category.id,
               service_location: 'My home'}

def add_task(task_creator, task_params)
  task = Tasks::Create.new(task_creator, task_params).call
  puts "Added task:  #{task.title} \n \n"
end

Task.destroy_all
add_task(task_creator, task_params)
add_task(task_creator, task_params.merge({title: 'Task with approved requests'}))
add_task(task_creator, task_params.merge({title: 'Task without requests'}))

20.times do
  add_task(task_creator, task_params.merge({title: Faker::Lorem.sentence(word_count: rand(3..5))}))
end

puts "Tasks added \n \n"