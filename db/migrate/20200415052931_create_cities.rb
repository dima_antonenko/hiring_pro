class CreateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :cities do |t|
      t.string :title, index: true
      t.integer :position, index: true, null: false, default: 0
      t.boolean :deleted, index: true, null: false, default: false

      t.string :meta_title, index: true
      t.text   :meta_description, index: true
      t.text   :meta_keywords, index: true

      t.timestamps
    end
    add_index :cities, :created_at
    add_index :cities, :updated_at
  end
end
