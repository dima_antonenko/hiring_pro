class Api::V1::AttachmentsController < ApplicationController
  before_action :authenticate_user_from_token!

  def create
    result = Attachments::Base.new(current_user, prm[:item_type], prm[:item_id],
                                    attachments: prm[:attachments], attachment_id: nil).create
    render_response(result)
  end

  def destroy
    result = Attachments::Base.new(current_user, prm[:item_type], prm[:item_id],
                                   attachments: nil, attachment_id: params[:id]).destroy
    render_response(result)
  end

  private

  def prm
    params.require(:attachment).permit(:item_type, :item_id, :id, attachments: [])
  end
end
