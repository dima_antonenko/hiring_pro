module Avatars
  class Base < BaseService
    require "image_processing/vips"
    include UserValidations
    include TaskValidations
    include ImageValidations

    attr_reader :current_user, :item_type, :item_id, :image, :coordinates

    def initialize(current_user, item_type, item_id, image:, coordinates:)
      @current_user, @image = current_user, image
      @item_type, @item_id = item_type, item_id
      @coordinates = coordinates
    end

    def create
      case item_type
      when 'task'
        Avatars::Tasks::Create.new(current_user, item_id, image).call
      when 'user'
        Avatars::Users::Create.new(current_user, image, coordinates).call
      else
        ForbiddenError.new('invalid item_type')
      end
    end

    def update
      case item_type
      when 'task'
        Avatars::Tasks::Update.new(current_user, item_id, coordinates).call
      when 'user'
        Avatars::Users::Update.new(current_user, coordinates).call
      else
        ForbiddenError.new('invalid item_type')
      end
    end

    def destroy
      case item_type
      when 'task'
        Avatars::Tasks::Destroy.new(current_user, item_id).call
      else
        ForbiddenError.new('invalid item_type')
      end
    end

    protected

    def crop(record, coordinates)
      coordinates.delete_if { |k, v| v.nil? }
      return nil if coordinates == {}

      pipline = ImageProcessing::Vips.source(File.open(record.avatar_full_path))
      tempfile = pipline.crop(coordinates[:x],
                              coordinates[:y],
                              coordinates[:width],
                              coordinates[:height]).call

      record.avatar.attach(
        io: tempfile,
        filename: "cropped_#{coordinates[:x]}_#{coordinates[:y]}_#{coordinates[:width]}_#{coordinates[:height]}.png",
        content_type: 'png',
        identify: false
      )

    end

    def check_and_save(record, &block)
      if record.avatar.attached?
        block&.call if block_given?
        save_and_return(record)
      else
        ForbiddenError.new('Image not attached')
      end
    end
  end
end
