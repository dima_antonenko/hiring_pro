module Avatars
  module Tasks
    class Update < Avatars::Base
      attr_reader :current_user, :task, :coordinates #{x, y, width, height}

      def initialize(current_user, task_id, coordinates)
        @current_user, @coordinates = current_user, coordinates
        @task = Task.find_by(id: task_id)
      end

      def call
        validate!
        assign!
        save_and_return(task)
      end

      private

      # ImageProcessing::Vips.crop(20, 50, 300, 300)
      # -> extracts 300x300 area with top-left edge 20,50
      def cropped_avatar
        pipline = ImageProcessing::Vips.source(File.open(task.avatar_full_path))
        pipline.crop(coordinates[:x],
                     coordinates[:y],
                     coordinates[:width],
                     coordinates[:height])
        pipline.call
      end

      def assign!
        cropped_avatar_path = cropped_avatar.path
        task.avatar.attach(
            io: File.open(cropped_avatar_path),
            filename: 'cropped.png',
            content_type: 'png',
            identify: false
          )
      end

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        avatar_attached(task)
        user_active?(current_user)
        task_not_deleted?(task)
        task_has_valid_completion?(task)
        user_has_access?(current_user, task)
        task_not_approved?(task)
      end
    end
  end
end
