module Profile
  class ContentLimits
    include TaskValidations
    include TaskRequestValidations
    include ReviewValidations
    include MoneyValidations

    def call
      {task: task,
       review: review}
    end

    private

    def task
      {max_execution_time: TaskValidations::MAX_EXECUTION_TIME,
       max_create_review_time: TaskValidations::MAX_CREATE_REVIEW_TIME}
    end

    def review
      {review_validations: ReviewValidations::MAX_UPDATE_TIME}
    end

    def money
      {
          max_amount: MoneyValidations::MAX_AMOUNT,
          min_amount: MoneyValidations::MIN_AMOUNT,
          types_transaction: MoneyValidations::TYPES_TRANSACTION
      }
    end
  end
end
