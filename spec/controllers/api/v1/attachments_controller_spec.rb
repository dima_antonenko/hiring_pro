require 'rails_helper'

RSpec.describe Api::V1::AttachmentsController, type: :request do
  let! (:task_creator) { create(:user) }
  let! (:other_user) { create(:user) }

  let! (:task) { create(:task, user_id: task_creator.id) }

  let! (:attachments) { [ fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'attachments/png')] }

  def add_attachments(user, item_type, item_id, attachments)
    post api_v1_attachments_path, params: {attachment: {item_type: item_type, item_id: item_id, attachments: attachments}},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def delete_attachment(user, item_type, item_id, attachment_id)
    delete api_v1_attachment_path(attachment_id), params: {attachment: {item_type: item_type, item_id: item_id}},
           headers: auth_headers(user)
    JSON.parse(response.body)
  end


  # create attachments
  describe 'POST /api/v1/attachments' do
    describe 'add attachments to task' do
      context 'positive scenarios' do
        it 'user can add attachments to task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
        end

        it 'user can add two attachments to task' do
          add_attachments(task_creator, 'task', task.id, attachments)
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].size).to eq(2)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t add attachments to task' do
          add_attachments(nil, 'task', task.id, attachments)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add attachments to invalid item type' do
          add_attachments(task_creator, 'test', task.id, attachments)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add attachments to invalid task' do
          add_attachments(nil, 'task', 0, attachments)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add attachments to not his task' do
          task.update_attribute(:user_id, other_user.id)
          add_attachments(task_creator, 'task', task.id, attachments)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add attachments to deleted task' do
          task.update_attribute(:deleted, true)
          add_attachments(task_creator, 'task', task.id, attachments)
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  # delete attachment
  describe 'DELETE /api/v1/attachment/' do
    describe 'delete image from task' do
      context 'positive scenarios' do
        it 'user can remove image from task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          json = delete_attachment(task_creator, 'task', task.id, json['data']['attributes']['attachments'].first['id'])
          expect(json['data']['attributes']['attachments']).to be_empty
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t delete image from task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          json = delete_attachment(nil, 'task', task.id, json['data']['attributes']['attachments'].first['id'])
          expect(response).to have_http_status(422)
        end

        it 'user can`t add remove image from invalid item_type' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          json = delete_attachment(task_creator, 'test', task.id, json['data']['attributes']['attachments'].first['id'])
        end

        it 'user can`t remove image from invalid task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          json = delete_attachment(task_creator, 'task', 0, json['data']['attributes']['attachments'].first['id'])
          expect(response).to have_http_status(422)
        end


        it 'user cannot add image to not his task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          task.update_attribute(:user_id, other_user.id)
          json = delete_attachment(task_creator, 'task', task.id, json['data']['attributes']['attachments'].first['id'])
          expect(response).to have_http_status(422)
        end

        it 'user cannot add image to deleted task' do
          json = add_attachments(task_creator, 'task', task.id, attachments)
          expect(json['data']['attributes']['attachments'].empty?).to be_falsey
          task.update_attribute(:deleted, true)
          json = delete_attachment(task_creator, 'task', task.id, json['data']['attributes']['attachments'].first['id'])
          expect(response).to have_http_status(422)
        end
      end
    end
  end
end
