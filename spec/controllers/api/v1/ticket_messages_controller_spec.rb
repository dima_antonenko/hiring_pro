require 'rails_helper'
RSpec.describe Api::V1::TicketMessagesController, type: :request do
  let! (:user) { create(:user) }
  let! (:ticket) {create(:ticket, user_id: user.id)}
  let! (:ticket_message_params) { {ticket_message: attributes_for(:ticket_message).merge({ticket_id: ticket.id})} }

  def create_message(user, params)
    post api_v1_ticket_messages_path, params: params, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # create
  describe 'POST /api/v1/ticket_messages/' do
    describe 'positive scenarios' do
      it 'registred user can create ticket_message' do
        json = create_message(user, ticket_message_params)
        expect(json['data']['attributes']['content']).to eq(ticket_message_params[:ticket_message][:content])
      end
    end

    describe 'negative scenarios' do
      it 'unregistred user can`t create ticket_message' do
        json = create_message(nil, ticket_message_params)
        expect(response).to have_http_status(422)
      end

      it 'user can`t create invalid messages qty' do
        create_list(:ticket_message, 101, user_id: user.id, ticket_id: ticket.id)
        json = create_message(user, ticket_message_params)
        expect(response).to have_http_status(422)
      end

      it 'user can`t create for invalid params' do
        prm = ticket_message_params
        prm[:ticket_message][:content] = ''
        json = create_message(user, ticket_message_params)
        expect(response).to have_http_status(422)
      end
    end
  end
end
