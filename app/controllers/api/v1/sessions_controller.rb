class Api::V1::SessionsController < ::Devise::SessionsController
  def create
    if request.format.json?
      super do |user|
        data = {
          token: user.authentication_token,
          email: user.email,
          id: user.id
        }
        render json: data, status: 201 and return
      end
    else
      super
    end
  end

  def sign_in_params
    if request.post?
      if request.format.json?
        params.require(:session).require(:user).permit(:email, :password)
      else
        params.permit(:user).permit(:email, :password)
      end
    end
  end
end