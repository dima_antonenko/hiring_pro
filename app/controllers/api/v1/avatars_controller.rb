class Api::V1::AvatarsController < ApplicationController
  before_action :authenticate_user_from_token!

  def create
    result = Avatars::Base.new(current_user, prm[:item_type], prm[:item_id],
                               image: prm[:image], coordinates: coordinates).create

    render json: CurrentUserSerializer.new(result)
    # render_response(result)
    # render json: { data: { avatar: result.avatar_full_path} }
  end

  def update
    result = Avatars::Base.new(current_user, prm[:item_type], prm[:item_id],
                               coordinates: coordinates,
                               image: nil).update
    render_response(result)
  end

  def destroy
    result = Avatars::Base.new(current_user, prm[:item_type], prm[:item_id],
                               image: nil, coordinates: nil).destroy
    render_response(result)
  end

  private

  def coordinates
    { x: prm[:crop_x].to_i, y: prm[:crop_y].to_i, width: prm[:crop_w].to_i, height: prm[:crop_h].to_i }
  end

  def prm
    params.require(:image).permit(:item_type, :item_id, :image, :crop_x, :crop_y, :crop_w, :crop_h)
  end
end
