require 'rails_helper'

RSpec.describe Api::V1::PasswordsController, type: :request do

  let! (:user) { create(:user) }
  let! (:user_params) {{user: {old_password: 'password', new_password: 'new_password', confirm_password: 'new_password'}}}

  def update_password(params)
    path user_password_path, params: params, headers: auth_headers(user)
  end

  # update password
  # describe 'PATCH /user/passwords' do
  #   context 'positive scenarios' do
  #     it 'user can update his password' do
  #       update_password(user_params)
  #       expect(response).to have_http_status(200)
  #     end
  #   end
  # end

end