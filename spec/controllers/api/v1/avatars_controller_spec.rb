require 'rails_helper'

RSpec.describe Api::V1::AvatarsController, type: :request do
  let! (:task_creator) { create(:user) }
  let! (:other_user) { create(:user) }

  let! (:task) { create(:task, user_id: task_creator.id) }

  let! (:image) { fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'image/png') }

  def add_avatar(user, item_type, item_id, image)
    post api_v1_avatars_path, params: {image: { item_type: item_type, item_id: item_id, image: image }},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def add_cropped_avatar(user, item_type, item_id, image)
    post api_v1_avatars_path, params: {image: {item_type: item_type, item_id: item_id, image: image,
                                               crop_x: 10, crop_y: 10, crop_w: 100, crop_h: 100}},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def delete_avatar(user, item_type, item_id)
    delete api_v1_avatar_path(0), params: {image: {item_type: item_type, item_id: item_id, image: image}},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def update_avatar(user, item_type, item_id)
    patch api_v1_avatar_path(0), params: {image: {item_type: item_type,
                                                   item_id: item_id,
                                                   crop_x: 10, crop_y: 10, crop_w: 100, crop_h: 100 }},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # create avatars
  describe 'POST /api/v1/avatars' do
    describe 'add image to task' do
      context 'positive scenarios' do
        it 'user can add image to task' do
          json = add_avatar(task_creator, 'task', task.id, image)
          expect(json['data']['attributes']['avatar_full_path'].include?('static_data/noimage')).to be_falsey
            # expect(response).to have_http_status(200)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t add image to task' do
          add_avatar(nil, 'task', task.id, image)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add image to invalid item type' do
          add_avatar(task_creator, 'test', task.id, image)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add image to invalid task' do
          add_avatar(task_creator, 'task', 99, image)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid image to task' do
          add_avatar(task_creator, 'task', task.id, nil)
          expect(response).to have_http_status(500)
        end

        it 'user cannot add image to not his task' do
          task.update_attribute(:user_id, other_user.id)
          add_avatar(task_creator, 'task', task.id, image)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add image to deleted task' do
          task.update_attribute(:deleted, true)
          add_avatar(task_creator, 'task', task.id, image)
          expect(response).to have_http_status(422)
        end
      end
    end

    describe 'add image to user' do
      context 'positive scenarios' do
        it 'user can add image to his profile' do
          json = add_avatar(task_creator, 'user', nil, image)
          expect(json['data']['attributes']['avatar_full_path'].include?('static_data/noimage')).to be_falsey
          expect(response).to have_http_status(200)
        end

        it 'user can add image with crop params to his profile' do
          json = add_cropped_avatar(task_creator, 'user', nil, image)
          expect(json['data']['attributes']['avatar_full_path'].include?('cropped')).to be_truthy
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t add image to profile' do
          add_avatar(nil, 'user', nil, image)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid image to profile' do
          add_avatar(task_creator, 'user', nil, nil)
          expect(response).to have_http_status(500)
        end

        it 'deleted user can`t add image to profile' do
          task_creator.update_attribute(:deleted, true)
          add_avatar(task_creator, 'user', nil, image)
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  # update avatars
  describe 'POST /api/v1/avatars/' do
    describe 'update image from task' do
      context 'positive scenarios' do
        it 'user can crop image from his task' do
          add_avatar(task_creator, 'task', task.id, image)
          json = update_avatar(task_creator, 'task', task.id)
          expect(json['data']['attributes']['avatar_full_path'].include?('cropped.png')).to be_truthy
        end
      end

      context 'negative scenarios' do
        it 'user cant crop not attached avatar' do
          json = update_avatar(task_creator, 'task', task.id)
          expect(json['errors']['avatar'].first).to eq('Avatar not attached')
        end
      end
    end

    describe 'update image from user' do
      context 'positive scenarios' do
        it 'user can crop image from his profile' do
          add_avatar(task_creator, 'user', 0, image)
          json = update_avatar(task_creator, 'user', 0)
          expect(json['data']['attributes']['avatar_full_path'].include?('cropped')).to be_truthy
        end
      end

      context 'negative scenarios' do
        it 'user cant crop not attached avatar' do
          json = update_avatar(task_creator, 'user', 0)
          expect(json['errors']['avatar'].first).to eq('Avatar not attached')
        end
      end
    end
  end

  # delete avatars
  describe 'DELETE /api/v1/avatars/' do
    describe 'delete image from task' do
      context 'positive scenarios' do
        it 'user can remove image from task' do
          add_avatar(task_creator, 'task', task.id, image)
          json = delete_avatar(task_creator, 'task', task.id)
          expect(json['data']['attributes']['avatar_full_path']).to eq('/static_data/noimage/noimage.png')
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t delete image from task' do
          add_avatar(task_creator, 'task', task.id, image)
          delete_avatar(nil, 'task', task.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add remove image from invalid item_type' do
          add_avatar(task_creator, 'task', task.id, image)
          delete_avatar(task_creator, 'test', task.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t remove image from invalid task' do
          add_avatar(task_creator, 'task', task.id, image)
          delete_avatar(task_creator, 'test', task.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t delete not existing image from task' do
          add_avatar(task_creator, 'task', task.id, image)
          task.avatar.purge
          task.save
          delete_avatar(task_creator, 'task', task.id)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add image to not his task' do
          add_avatar(task_creator, 'task', task.id, image)
          task.update_attribute(:user_id, other_user.id)
          delete_avatar(task_creator, 'task', task.id)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add image to deleted task' do
          add_avatar(task_creator, 'task', task.id, image)
          task.update_attribute(:deleted, true)
          delete_avatar(task_creator, 'task', task.id)
          expect(response).to have_http_status(422)
        end
      end
    end
  end
end
