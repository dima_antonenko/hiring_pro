module Avatars
  module Tasks
    class Create < Avatars::Base
      attr_reader :current_user, :task, :image

      def initialize(current_user, task_id, image)
        @current_user, @image = current_user, image
        @task = Task.find_by(id: task_id)
      end

      def call
        validate!
        task.avatar.attach(image)
        check_and_save(task)
      end

      private

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        image_exist?(image)
        user_active?(current_user)
        task_not_deleted?(task)
        task_has_valid_completion?(task)
        user_has_access?(current_user, task)
        task_not_approved?(task)
      end
    end
  end
end