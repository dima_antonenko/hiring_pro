City.destroy_all
cities = ['Lusaka', 'Kitwe', 'Ndola', 'Kabwe', 'Chingola', 'Mufulira', 'Livingstone', 'Luanshya']
cities.each do |city_name|
  City.create(title: city_name, position: cities.find_index(city_name))
end

puts "Cities added \n \n"