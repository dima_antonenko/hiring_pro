class Api::V1::TicketsController < ApplicationController
  before_action :authenticate_user_from_token!
  before_action :get_ticket, except: [:index, :create]

  def index
    result = Tickets::Index.new(current_user, params).call
    render json: TicketSerializer.new(result)
  end

  def create
    result = Tickets::Create.new(current_user, ticket_params).call
    render_response(result)
  end

  def destroy
    result = Tickets::Destroy.new(current_user, @ticket).call
    render_response(result)
  end

  def show
    result = Tickets::Show.new(current_user, @ticket).call
    render json: TicketSerializer.new(result)
  end

  private

  def ticket_params
    params.require(:ticket).permit(:subject, :description, :reason_type)
  end

  def get_ticket
    @ticket = Ticket.visible.find(params[:id])
  end
end