module Bookmarks
  module Cities
    class Create < Bookmarks::Base
      include CityValidations
      include BookmarksValidations

      attr_reader :current_user, :city

      def initialize(current_user, item_id)
        @current_user = current_user
        @city = City.find_by(id: item_id)
      end

      def call
        validate!
        current_user.bookmark_city_ids << city.id
        save_and_return(current_user)
      end

      private

      def validate!
        user_active?(current_user)
        city_exist?(city)
        city_not_deleted?(city)
        city_not_bookmarked?(current_user, city.id)
      end
    end
  end
end
