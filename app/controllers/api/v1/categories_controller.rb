class Api::V1::CategoriesController < ApplicationController
  def index
    result = Category.where(parent_id: nil).order("position asc").all
    render json: CategorySerializer.new(result, {include: ["categories"]}) if stale?(result)
  end
end
