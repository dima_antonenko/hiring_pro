Rails.application.routes.draw do
  devise_for :admin_users, controllers: { sessions: 'admin/sessions' }

  post '/api/v1/profile/update',         to: 'api/v1/profile#update',         as: 'update_profile'
  get  '/api/v1/profile/content_limits', to: 'api/v1/profile#content_limits', as: 'profile_content_limits'
  get  '/api/v1/profile/score',          to: 'api/v1/profile#score',          as: 'profile_score'

  get '/admin', to: redirect('/admin/users')
  get '/admin/dashboard', to: redirect('/admin/users')

  namespace :admin do
    resources :cities, except: [:show] do
      post 'restore', on: :member
    end

    resources :tasks
    resources :task_requests, except: [:show, :new, :create]
    resources :users do
      get 'real_money', on: :member
      get 'virtual_money', on: :member
    end
    resources :categories, except: [:show] do
      post 'restore', on: :member
    end
    resources :reviews, except: [:show, :new, :create]

    resources :tickets, only: [:index, :show, :destroy]
    resources :ticket_messages, only: [:create]
  end
end
