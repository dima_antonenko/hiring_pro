module ImageValidations
  def image_exist?(image)
    raise ForbiddenError.new(:image, 'Image not found') unless image
  end

  def attachments_exist?(images)
    raise ForbiddenError.new(:images, 'Attachments not found') if images.size == 0
  end

  def avatar_attached(record)
    raise ForbiddenError.new(:avatar, 'Avatar not attached') unless record.avatar.attached?
  end
end
