module Bookmarks
  module Categories
    class Create < Bookmarks::Base
      include CategoryValidations
      include BookmarksValidations

      attr_reader :current_user, :category

      def initialize(current_user, item_id)
        @current_user = current_user
        @category = Category.find_by(id: item_id)
      end

      def call
        validate!
        current_user.bookmark_category_ids << category.id
        save_and_return(current_user)
      end

      private

      def validate!
        user_active?(current_user)
        category_exist?(category)
        category_not_deleted?(category)
        category_not_root?(category)
        category_not_bookmarked?(current_user, category.id)
      end
    end
  end
end
