require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :request do
  before :each do
    @user_attributes = FactoryBot.attributes_for(:user)
    @user = User.create(@user_attributes)
  end

  def sign_in(email, password)
    post user_session_path, params: {user: {email: email, password: password } }, headers: base_headers
    JSON.parse(response.body)
  end

  # sign_in
  # describe 'POST /users/sign_in' do
  #   describe 'positive scenarios' do
  #     it 'user can sing_in' do
  #       sign_in(@user_attributes[:email], @user_attributes[:password])
  #       expect(response).to have_http_status(201)
  #     end
  #   end
  #
  #   describe 'negative scenarios' do
  #     it 'user cannot sing_in for invalid email' do
  #       sign_in('invalid_email@test.ru', 'password')
  #       expect(response).to have_http_status(401)
  #     end
  #
  #     it 'user cannot sing_in for invalid password' do
  #       sign_in(@user.email, 'invalid_password')
  #       expect(response).to have_http_status(401)
  #     end
  #
  #     it 'user cannot sing_in for empty email & password' do
  #       sign_in('', '')
  #       expect(response).to have_http_status(401)
  #     end
  #   end
  # end

end