require 'rails_helper'

RSpec.describe Api::V1::BookmarksController, type: :request do
  let! (:user) { create(:user) }

  let! (:city) { create(:city) }

  let! (:children_category) {create(:category, title: 'Дочерняя категория')}
  let! (:parent_category) { children_category.parent }

  def add(user, item_type, item_id)
    post api_v1_bookmarks_path, params: { item_type: item_type, item_id: item_id},
         headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def remove(user, item_type, item_id)
    delete api_v1_bookmark_path(0), params: {item_type: item_type, item_id: item_id},
           headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # create bookmarks
  describe 'POST /api/v1/bookmarks' do
    describe 'category' do
      context 'positive scenarios' do
        it 'user can category to bookmarks' do
          json = add(user, 'category', children_category.id)
          expect(json['data']['attributes']['bookmark_categories'].size).to eq(1)
          expect(response).to have_http_status(200)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t add category to bookmarks' do
          add(nil, 'category', children_category.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid item_type to bookmarks' do
          add(user, 'test', children_category.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid category_id' do
          add(user, 'category', 0)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add root category to bookmarks' do
          add(user, 'category', parent_category.id)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add deleted category to bookmarks' do
          children_category.update_attribute(:deleted, true)
          add(user, 'category', children_category.id)
          expect(response).to have_http_status(422)
        end
      end
    end

    describe 'city' do
      context 'positive scenarios' do
        it 'user can city to bookmarks' do
          json = add(user, 'city', city.id)
          expect(json['data']['attributes']['bookmark_cities'].size).to eq(1)
          expect(response).to have_http_status(200)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t add city to bookmarks' do
          add(nil, 'city', city.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid item_type to bookmarks' do
          add(user, 'test', city.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add invalid city_id' do
          add(user, 'category', 0)
          expect(response).to have_http_status(422)
        end

        it 'user cannot add deleted city to bookmarks' do
          city.update_attribute(:deleted, true)
          add(user, 'category', city.id)
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  # delete bookmarks
  describe 'DELETE /api/v1/bookmarks/' do
    describe 'category' do
      context 'positive scenarios' do
        it 'user can remove bookmark' do
          add(user, 'category', children_category.id)
          json = remove(user, 'category', children_category.id)
          expect(json['data']['attributes']['bookmark_categories'].size).to eq(0)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t delete category from user' do
          add(user, 'category', children_category.id)
          remove(nil, 'category', children_category.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add remove category from invalid item_type' do
          add(user, 'category', children_category.id)
          remove(user, 'test', children_category.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t remove invalid category_id' do
          add(user, 'category', children_category.id)
          remove(user, 'category', 0)
          expect(response).to have_http_status(422)
        end

        it 'user can`t delete not added category' do
          remove(user, 'category', children_category.id)
          expect(response).to have_http_status(422)
        end
      end
    end

    describe 'city' do
      context 'positive scenarios' do
        it 'user can remove bookmark' do
          json = add(user, 'city', city.id)
          json = remove(user, 'city', city.id)
          expect(json['data']['attributes']['bookmark_cities'].size).to eq(0)
        end
      end

      context 'negative scenarios' do
        it 'unregsitred user can`t delete city from user' do
          add(user, 'city', city.id)
          remove(nil, 'city', city.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t add remove city from invalid item_type' do
          add(user, 'city', city.id)
          remove(user, 'test', city.id)
          expect(response).to have_http_status(422)
        end

        it 'user can`t remove invalid city' do
          add(user, 'category', city.id)
          remove(user, 'category', 0)
          expect(response).to have_http_status(422)
        end

        it 'user can`t delete not added category' do
          remove(user, 'city', city.id)
          expect(response).to have_http_status(422)
        end
      end
    end
  end
end
