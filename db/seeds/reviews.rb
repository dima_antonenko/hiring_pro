@completed_task = Task.find_by(title: 'Completed task')
@request_creator = User.find_by(email: 'request_creator@test.ru')
@task_request = @completed_task.task_requests.where(state: :completed).first
@review_params = {task_id: @completed_task.id, description: 'review description', rating: 5}

Review.destroy_all
result = Reviews::Create.new(@request_creator, @review_params).call
puts "create review: #{result.attributes}"