Ticket.destroy_all

@user = User.first

def create_ticket(i)
  params = {subject: "Тестовый тикет #{i}",
            description: 'Здесь описание тикета',
            reason_type: '0'}
  Tickets::Create.new(@user, params).call
end

3.times do |t|
  create_ticket(t)
end

puts "Add tickets \n \n"