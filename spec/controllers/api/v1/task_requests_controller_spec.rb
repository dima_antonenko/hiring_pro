require 'rails_helper'

RSpec.describe Api::V1::TaskRequestsController, type: :request do
  let! (:task_creator) { create(:user) }
  let! (:user) { create(:user) }

  let! (:task) { create(:task, user_id: task_creator.id) }
  let! (:task_request_params) { attributes_for(:task_request).merge({task_id: task.id}) }
  let! (:task_request) { create(:task_request, task_id: task.id, user_id: user.id) }

  let! (:msg) { 'new-request-message' }

  def create_request(user, extra_params = nil)
    task_request_params.merge!(extra_params) if extra_params
    post api_v1_task_requests_path, params: {task_request: task_request_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # @todo использовать именованый вызов
  def update_request(user, request_id, extra_params = nil)
    task_request_params.merge!(extra_params) if extra_params
    patch api_v1_task_request_path(request_id), params: {task_request: task_request_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def delete_request(user, request_id)
    delete api_v1_task_request_path(request_id), headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def approve_request(user, request_id)
    post approve_api_v1_task_request_path(request_id), headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_task(user, task_id)
    get api_v1_task_path(task_id), params: {id: task_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_task_requests
    get api_v1_task_requests_path, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # index
  describe 'GET /api/v1/task_requests' do
    before(:each) do
      create_request(user)
    end
    context 'positive scenarios' do
      it 'user can his task_requests' do
        json = get_task_requests
        expect(json['data'][0]['attributes']['price']).to be_truthy
      end

      it 'user can his task_requests and related tasks' do
        json = get_task_requests
        expect(json['data'].first['relationships']['task']['data']['id']).to eq(task.id)
      end
    end
  end

  # create
  describe 'POST /api/v1/task_requests' do
    context 'positive scenarios' do
      it 'user can create task_request' do
        TaskRequest.destroy_all
        json = create_request(user)
        expect(response).to have_http_status(200)
      end

      it 'task has valid responded? attribute' do
        TaskRequest.destroy_all
        create_request(user)
        json = get_task(user, task.id)
        expect(json['data']['attributes']['responded_for_current_user']).to be_truthy
      end
    end

    context 'negative scenarious' do
      it 'unregistred user cannot add task_request' do
        create_request(nil)
        expect(response).to have_http_status(422)
      end

      it 'user cannot add task_request on his task' do
        create_request(task_creator)
        expect(response).to have_http_status(422)
      end

      it 'user cannot add task_request on deleted task' do
        task.update_attribute(:deleted, true)
        create_request(user)
        expect(response).to have_http_status(422)
      end

      it 'user cannot add task_request to not active task' do
        task.update_attribute(:completion_date, 1.day.ago)
        create_request(user)
        expect(response).to have_http_status(422)
      end

      it 'user cannot add two task_requests on task' do
        TaskRequest.destroy_all
        create_request(user)
        expect(response).to have_http_status(200)
        create_request(user)
        expect(response).to have_http_status(422)
      end

      it 'user cannot add task_request to invalid task' do
        create_request(user, {task_id: 0})
        expect(response).to have_http_status(422)
      end
    end
  end

  # update
  describe 'PATCH /api/v1/task_requests/:id' do
    context 'positive scenarios' do
      it 'user can update his task_request' do
        json = update_request(user, task_request.id, {message: msg})
        expect(json['data']['attributes']['message']).to eq(msg)
      end

      it 'user can create and update his task_request' do
        TaskRequest.destroy_all
        json = create_request(user)
        expect(response).to have_http_status(200)
        json = update_request(user, json['data']['id'], {message: msg})
        expect(json['data']['attributes']['message']).to eq(msg)
      end
    end

    context 'negative scenarios' do
      it 'unregistred user cannot update taks_request' do
        update_request(nil, task_request.id, {message: msg})
        expect(response).to have_http_status(422)
      end

      it 'user cannot update not his task_request' do
        json = update_request(task_creator, task_request.id)
        expect(response).to have_http_status(422)
      end


      it 'user cannot update request for not active task' do
        task.update_attribute(:deleted, true)
        update_request(user, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot update approved task_request' do
        task_request.approved!
        update_request(user, task_request.id)
        expect(response).to have_http_status(422)
      end
    end
  end

  # delete
  describe 'DELETE /api/v1/task_requests/:id' do
    context 'positive scenarios' do
      it 'user can delete his task_request' do
        json = delete_request(user, task_request.id)
        expect(json['data']['attributes']['deleted']).to be_truthy
      end

      it 'user can create  & delete his task_request' do
        TaskRequest.destroy_all
        json = create_request(user)
        expect(response).to have_http_status(200)
        json = delete_request(user, json['data']['id'])
        expect(json['data']['attributes']['deleted']).to be_truthy
      end
    end

    context 'negative scenarios' do
      it 'unregistred user cannot delete taks_request' do
        delete_request(nil, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot delete not his task_request' do
        delete_request(task_creator, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot delete request for not active task' do
        task.update_attribute(:deleted, true)
        delete_request(user, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot delete approved task_request' do
        task_request.approved!
        delete_request(user, task_request.id)
        expect(response).to have_http_status(422)
      end
    end
  end

  # approve task_request
  describe 'POST /api/v1/task_requests/:id/approve' do
    context 'positive scenarios' do
      it 'user can approve task request on his task' do
        json = approve_request(task_creator, task_request.id)
        expect(json['data']['attributes']['state']).to eq('approved')
      end
    end

    context  'negative scenarios' do
      it 'user can`t approve task request for not active task' do
        task_request.task.update({completion_date: 1.year.ago})
        approve_request(task_creator, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t approve deleted task_request' do
        task_request.update_attribute(:deleted, true)
        approve_request(task_creator, task_request.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t approve request for not his task' do
        other_user = create(:user)
        task.update_attribute(:user_id, other_user.id)
        approve_request(task_creator, task_request.id)
        expect(response).to have_http_status(422)
      end
    end
  end

  describe 'complex scenarios' do
    it 'create + update + destroy' do
      TaskRequest.destroy_all

      json = create_request(user)
      task_request_id = json['data']['id']
      expect(json['data']['id']).to be_truthy

      json = update_request(user, task_request_id, {message: msg})
      expect(json['data']['attributes']['message']).to eq(msg)

      json = delete_request(user, task_request_id)
      expect(json['data']['attributes']['deleted']).to be_truthy
    end

    it 'create + update + approve' do
      TaskRequest.destroy_all

      json = create_request(user)
      task_request_id = json['data']['id']
      expect(json['data']['id']).to be_truthy

      json = update_request(user, task_request_id, {message: msg})
      expect(json['data']['attributes']['message']).to eq(msg)

      json = approve_request(task_creator, task_request_id)
      expect(json['data']['attributes']['state']).to eq('approved')
    end
  end
end
