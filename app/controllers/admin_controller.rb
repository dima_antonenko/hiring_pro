class AdminController < ApplicationController
  layout 'admin'
  before_action :check_global_permissions

  def record_save?(result)
    result.is_a?(ActiveRecord::Base)
  end

  private

  def check_global_permissions
    redirect_to new_admin_user_session_path unless current_admin_user
  end
end
