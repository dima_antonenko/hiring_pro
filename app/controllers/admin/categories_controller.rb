class Admin::CategoriesController < AdminController
  before_action :get_category, only: [:edit, :update, :destroy, :restore]

  def index
    @all_categories = Category.all
  end

  def new
    @new_category = Category.new
    @parent_categories = get_parent_categories
  end

  def create
    result = Admins::Categories::Create.new(category_params, params[:category][:category_id]).call
    if record_save?(result)
      flash[:success] = "Запись создана"
      redirect_to edit_admin_category_path(result)
    else
      flash[:errors] = result
      redirect_back(new_admin_category_path)
    end
  end

  def update
    @category.assign_attributes(category_params)
    @category.save ? flash[:success] = "Запись обновлена" : flash[:errors] = @category.errors.full_messages
    redirect_back(fallback_location: admin_categories_path)
  end

  def edit; end

  def destroy
    @category.update_attribute(:deleted, true)
    redirect_back(fallback_location: admin_categories_path)
  end

  def restore
    @category.update_attribute(:deleted, false)
    redirect_back(fallback_location: admin_categories_path)
  end

  private

  def get_category
    @category = Category.find(params[:id])
  end

  def get_parent_categories
    categories = Category.roots.collect{ |category| [category.title, category.id] }
    categories.unshift(['No parent category', 0])
  end

  def category_params
    params.require(:category).permit(:title, :system_name, :position)
  end
end
