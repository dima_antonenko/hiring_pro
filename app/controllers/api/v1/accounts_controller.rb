class Api::V1::AccountsController < ApplicationController
  before_action :authenticate_user_from_token!
  
  def update
    current_user.update(user_params)
    render json: CurrentUserSerializer.new(current_user)
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :phone, :city_id)
  end
end
