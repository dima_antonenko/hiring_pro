module Tickets
  class Create < BaseService
    include UserValidations
    include TicketValidations

    attr_accessor :current_user, :ticket, :params

    def initialize(current_user, params)
      @current_user = current_user
      @params = params
      @ticket = Ticket.new
    end

    def call
      validate!
      add
      save_and_return(ticket)
    end

    def add
      reason_type = params[:reason_type].to_i
      params.extract!(:reason_type)
      ticket.user_id = current_user.id
      ticket.assign_attributes(params.merge(reason_type: reason_type))
    end

    private

    def validate!
      user_active?(current_user)
      user_ticket_limits_valid?(current_user)
    end
  end
end