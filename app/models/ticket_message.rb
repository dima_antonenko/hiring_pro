class TicketMessage < ApplicationRecord
  belongs_to :ticket
  belongs_to :user, optional: true
  belongs_to :admin_user, optional: true

  validates :content, presence: true
end
