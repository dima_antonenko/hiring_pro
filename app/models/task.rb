class Task < ApplicationRecord
  include PgSearch::Model

  has_one_attached :avatar
  has_many_attached :attachments

  has_many :task_requests
  has_many :reviews

  belongs_to :category
  belongs_to :user
  belongs_to :city

  enum state: { active: 0, performed: 1, completed: 2 }
  enum payment_type: { card: 0, cash: 1 }

  validates :title, presence: true, length: {minimum: 5, maximum: 400}
  validates :description,           length: {maximum: 5000}
  validates :service_location,      length: {maximum: 150}
  validates :price, presence: true, format: { with: /\A\d+(?:\.\d{2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
  validates :payment_type, presence: true
  validate  :category_not_root?

  scope :visible, -> { where(deleted: false).where('completion_date > ?', DateTime.now).order('created_at DESC') } # возможно добавить with_zone
  pg_search_scope :search_everywhere, against: [:title, :description]

  def avatar_full_path
    ActiveStorage::Blob.service.path_for(self.avatar.key)
  end

  private

  def category_not_root?
    errors.add(:task, "Related category can't be root") if self.category&.root?
  end
end
