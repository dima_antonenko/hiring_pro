ip = '5.9.124.235'

server ip, user: 'deploy', roles: %w(web app db), port: 1022

set :stage, 'production'
set :rails_env, 'production'