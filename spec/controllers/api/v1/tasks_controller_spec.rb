require 'rails_helper'

RSpec.describe Api::V1::TasksController, type: :request do
  let! (:user) { create(:user) }
  let! (:second_user) { create(:user) }

  let! (:children_category) {create(:category, title: 'Дочерняя категория')}
  let! (:parent_category) { children_category.parent }
  let! (:task_params) {attributes_for(:task).merge({category_id: children_category.id})}

  let! (:task) {create(:task, user_id: user.id, category_id: children_category.id)}

  let! (:task_request) {create(:task_request, task_id: task.id, state: 2)}
  let! (:attachments) { [ fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'attachments/png')] }
  let! (:avatar) { fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'attachments/png') }

  def create_task(extra_params = {})
    prm = task_params
    if extra_params
      prm.merge!(extra_params)
    end
    post api_v1_tasks_path, params: {task: prm}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def update_task(task_id, extra_params = nil)
    task_params.merge!(extra_params) if extra_params
    patch api_v1_task_path(task_id), params: {task: task_params}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # добавить опциональную авторизацию
  def delete_task(task_id)
    delete api_v1_task_path(task_id), params: {id: task_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def complete_task(user, task_id)
    post complete_api_v1_task_path(task_id), params: {id: task_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_task(task_id, user = nil)
    get api_v1_task_path(task_id), params: {id: task_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_tasks(extra_params = {}, user = nil)
    get api_v1_tasks_path, params: ({}).merge(extra_params), headers: auth_headers(user)
    json = JSON.parse(response.body)
  end

  def get_newsfeed(extra_params = {}, user = nil)
    get api_v1_newsfeed_path, params: ({}).merge(extra_params), headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # create task
  describe 'POST /api/v1/tasks' do
    context 'positive scenarios' do
      it 'user can create task' do
        json = create_task
        expect(response).to have_http_status(200)
      end

      it 'user can create task without start_date' do
        task_params.extract!(:start_date)
        json = create_task
        expect(json['data']['attributes']['start_date']).to be_falsey
      end

      it 'user can create task with start_date and completion_date' do
        start_date = DateTime.now + 3.days
        completion_date = DateTime.now + 20.days
        json = create_task({start_date: start_date, completion_date: completion_date})
        expect(json['data']['attributes']['start_date'].to_date).to eq(start_date.to_date)
        expect(json['data']['attributes']['completion_date'].to_date).to eq(completion_date.to_date)
      end

      it 'user can create task with attachments' do
        json = create_task({ attachments: attachments, avatar: avatar })
        expect(json['data']['attributes']['avatar_full_path']).to be_truthy
        expect(json['data']['attributes']['attachments']).not_to be_empty
      end
    end

    context 'negative scenarios' do
      it 'user cannot create task for root category' do
        json =  create_task({category_id: parent_category.id })
        expect(response).to have_http_status(422)
      end

      it 'user cannot create task for invalid payment_type' do
        json = create_task({payment_type: 99 })
        expect(response).to have_http_status(422)
      end

      it 'user cannot create task for duration more than 1 year' do
        create_task({completion_date: DateTime.now + 2.year })
        expect(response).to have_http_status(422)
      end

      it 'unauthorized user cannot create task' do
        post api_v1_tasks_path, params: {task: task_params.merge({category_id: parent_category.id })}
        expect(response).to have_http_status(422)
      end
    end

    context 'fix bugs' do
      it 'creating task withot category' do
        json = create_task({category_id: nil})
        expect(response).to have_http_status(422)
      end

      it 'creating task withot completion_date' do
        json = create_task({completion_date: nil})
        expect(response).to have_http_status(200)
      end

      it 'creating task without category & completion_date' do
        json = create_task({completion_date: nil})
        expect(response).to have_http_status(200)
      end

      it 'creating task without params' do
        post api_v1_tasks_path, params: {task: {completion_date: nil}}, headers: auth_headers(user)
        json = JSON.parse(response.body)
        expect(response).to have_http_status(422)
      end
    end
  end

  # index
  describe 'GET /api/v1/tasks' do
    describe 'positive scenarios' do
      it 'get empty tasks' do
        get api_v1_tasks_path
        json = JSON.parse(response.body)
        expect(response).to have_http_status(200)
      end

      it 'get tasks' do
        create_task #create(:task) потому, что ругается на валидацию
        get api_v1_tasks_path
        expect(response).to have_http_status(200)
      end

      it 'get all tasks with categories' do
        create_task #create(:task)

        get api_v1_tasks_path, params: {include: "category"}
        expect(response).to have_http_status(200)

        json = JSON.parse(response.body)
        #binding.pry
        json['included'].count {|j| j['type'] == 'category'} > 0
          # expect(json['included'].count {|j| j['type'] == 'category'}).to be > 0 # возможно есть смысл дополнить так
      end

      it 'get all tasks with categories and users' do
        create_task #create(:task) потому, что ругается на валидацию

        get api_v1_tasks_path, params: {include: "category,user"}
        expect(response).to have_http_status(200)

        json = JSON.parse(response.body)
        json['included'].count {|j| j['type'] == 'category'} > 0
        json['included'].count {|j| j['type'] == 'user'} > 0
      end

      it 'user cannot get not active tasks' do
        task.update_attribute(:completion_date, 1.day.ago)
        get api_v1_tasks_path
        json = JSON.parse(response.body)
        expect(json['data']).to be_empty
      end

      it 'filter city' do
        valid_city = create(:city)
        valid_task = create(:task, city_id: valid_city.id)

        json = get_tasks({city_id: valid_city.id})
        expect(json['data'].size).to eq(1)
        expect(json['data'][0]['id']).to eq(valid_task.id.to_s)
      end

      it 'filter category' do
        valid_task = create(:task)

        json = get_tasks({category_id: valid_task.category_id})
        expect(json['data'].size).to eq(1)
        expect(json['data'][0]['relationships']['category']['data']['id']).to eq(valid_task.category_id.to_s)
      end

      it 'sorting price' do
        expensive_task = create(:task, price: 1000)
        cheap_task = create(:task, price: 100)


        json = get_tasks({order: 'price'})
        expect(json['data'][0]['id']).to eq(cheap_task.id.to_s)
      end

      it 'search query' do
        good_task = create(:task, title: 'good task')
        bad_task = create(:task, title: 'bad task')

        json = get_tasks({s_query: 'good'})

        expect(json['data'][0]['id']).to eq(good_task.id.to_s)
      end

      it 'paginate' do
        create_list(:task, 11)
        json = get_tasks
        expect(json['data'].size).to eq(10)
      end

      it 'paginate with current_page' do
        create_list(:task, 11)
        json = get_tasks({per_page: 10,  page: 2})
        expect(json['data'].size).to eq(2)
      end
    end

    describe 'get my tasks' do
      it 'user can get his tasks' do
        json = create_task
        taks_id = json['data']['id']
        json = get_tasks({my: true}, user)
        expect(json['data'].last['id']).to eq(taks_id)
      end
    end
  end

  # index
  describe 'GET /api/v1/newsfeed' do
    it 'user can get newsfeed' do
      create(:task, city_id: user.city_id, category_id: children_category.id)
      json = get_newsfeed({}, user)
      expect(json['data'].size).to eq(1)
    end

    it 'ungregistred user cant get newsfeed' do
      create(:task, city_id: user.city_id, category_id: children_category.id)
      json = get_newsfeed({}, nil)
      expect(response).to have_http_status(422)
    end
  end

  # update task
  describe 'PATCH /api/v1/tasks/:id' do
    before(:each) do
      TaskRequest.destroy_all
      task.active!
    end
    context 'positive scenarios' do
      it 'user can update his task' do
        new_title = 'new-task-title'
        json = update_task(task.id, {title: new_title})
        expect(json['data']['attributes']['title']).to eq(new_title)
      end

      it 'user can update city_id from task' do
        new_city = create(:city)
        json = update_task(task.id, {city_id: new_city.id})
        expect(json['data']['relationships']['city']['data']['id']).to eq(new_city.id.to_s)
      end
    end

    context 'negative scenarios' do
      it 'user cannot update not his record' do
        task.update_attribute(:user_id, second_user.id)
        update_task(task.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot update not existing record' do
        update_task(0)
        expect(response).to have_http_status(404)
      end

      it 'user cannot update deleted record' do
        task.update_attribute(:deleted, true)
        update_task(task.id)
        expect(response).to have_http_status(404)
      end

      it 'user cannot update locked attributes' do
        payment_type = '1'
        json = update_task(task.id, {payment_type: payment_type})
        expect(json['data']['attributes']['payment_type']).not_to eq(payment_type)
      end

      it 'user cannot update old task' do
        task.update_attribute(:completion_date, 1.day.ago)
        update_task(task.id)
        expect(response).to have_http_status(404)
      end

      it 'unregistred user cannot update task' do
        patch api_v1_task_path(task.id), params: {task: task_params}
        expect(response).to have_http_status(422)
      end
    end
  end

  # destroy
  describe 'DELETE /api/v1/tasks/:id' do
    before(:each) do
      TaskRequest.destroy_all
      task.active!
    end

    describe 'positive scenarios' do
      it 'user can delete his task' do
        json = delete_task(task.id)
        expect(json['data']['attributes']['deleted']).to be_truthy
      end
    end

    describe 'negative scenarious' do
      it 'unregistred user cannot delete task' do
        delete api_v1_task_path(task.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot delete not his task' do
        task.update_attribute(:user_id, second_user.id)
        delete_task(task.id)
        expect(response).to have_http_status(422)
      end

      it 'user cannot delete deleted task' do
        task.update_attribute(:deleted, true)
        delete_task(task.id)
        expect(response).to have_http_status(404)
      end

      it 'user cannot delete not existing task' do
        delete_task(0)
        expect(response).to have_http_status(404)
      end

      it 'user cannot delete task if task has approved task_requests' do
        task.performed!
        delete_task(task.id)
        expect(response).to have_http_status(422)
      end
    end
  end

  # complete
  describe 'POST /api/v1/tasks/:id/complete' do
    describe 'positive scenarios' do
      it 'user can complete his task' do
        task.task_requests.first.approved!
        json = complete_task(user, task.id)
        expect(json['data']['attributes']['state']).to eq('completed')
      end
    end

    describe 'negative scenarios' do
      it 'user can`t complete task without approved task_requests' do
        json = complete_task(user, task.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t complete not_his task' do
        other_user = create(:user)
        task.task_requests.first.approved!
        json = complete_task(other_user, task.id)
        expect(response).to have_http_status(422)
      end
    end
  end

  # show
  describe 'GET /api/v1/tasks/:id' do
    describe 'positive scenarios' do
      it 'user can get task' do
        json = get_task(task.id)
        expect(json['data']['id']).to eq(task.id)
      end

      it 'task get valid | state responded_for_current_user? for unregistred user' do
        json = get_task(task.id)
        expect(json['data']['attributes']['responded_for_current_user']).to eq('user not authorized')
      end

      it 'task get valid | state responded_for_current_user? for registred user' do
        json = get_task(task.id, user)
        expect(json['data']['attributes']['responded_for_current_user']).to eq(false)
      end
    end

    describe 'negative scenarios' do
      it 'user can`t get deleted task' do
        task.update_attribute(:deleted, true)
        json = get_task(task.id)
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'complex scenarios' do
    it 'create + update + add_image + add_request + approve + complete' do
      # create
      json = create_task
      expect(json['data']['id']).to be_truthy
      task_id = json['data']['id']

      # update
      json = update_task(json['data']['id'], {title: 'New title'})
      expect(json['data']['attributes']['title']).to eq('New title')

      # add_image
      image =  fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'image/png')
      post api_v1_avatars_path, params: {image: {item_type: 'task', item_id: task_id,
                                                image: image}}, headers: auth_headers(user)
      json = JSON.parse(response.body)
      expect(json['data']['attributes']['avatar_full_path'].include?('/rails/active_storage/')).to be_truthy

      # create_request
      request_creator = create(:user)
      post api_v1_task_requests_path, params: {task_request: attributes_for(:task_request)
                                                                 .merge({task_id: task_id})},
                                      headers: auth_headers(request_creator)
      json = JSON.parse(response.body)
      expect(json['data']['attributes']['state']).to eq('active')

      # approve_request
      post approve_api_v1_task_request_path(TaskRequest.last.id), headers: auth_headers(user)
      json = JSON.parse(response.body)
      expect(json['data']['attributes']['state']).to eq('approved')

      # complete
      json = complete_task(user, task_id)
      expect(json['data']['attributes']['state']).to eq('completed')
    end

    it 'create + update + add_image + destroy' do
      # create
      json = create_task
      expect(json['data']['id']).to be_truthy
      task_id = json['data']['id']

      # update
      json = update_task(json['data']['id'], {title: 'New title'})
      expect(json['data']['attributes']['title']).to eq('New title')

      # add_image
      image =  fixture_file_upload(Rails.root.join('public/static_data/noimage', 'noimage.png'), 'image/png')
      post api_v1_avatars_path, params: {image: {item_type: 'task', item_id: task_id,
                                                image: image}}, headers: auth_headers(user)
      json = JSON.parse(response.body)
      expect(json['data']['attributes']['avatar_full_path'].include?('/rails/active_storage/')).to be_truthy

      # destroy
      json = delete_task(task_id)
      expect(json['data']['attributes']['deleted']).to be_truthy
    end
  end
end
