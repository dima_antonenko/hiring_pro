
# city = City.create!(title: "Lusaka")
# category = Category.create!(title: "Some category", system_name: "some")
# child_category = Category.create!(title: "Some child category", system_name: "some", parent: category)
#
#
# u = User.create!(email: "denis@f7.ru", password: "12345678", password_confirmation: "12345678", city: city)
# u.tasks.create!(title: "some title",
#                 description: "description",
#                 price: 123,
#                 category: child_category,
#                 city: city,
#                 completion_date: 10.days.from_now,
#                 payment_type: Task::payment_types['card'])


Rake::Task["db:seed:categories"].invoke
Rake::Task["db:seed:cities"].invoke
Rake::Task["db:seed:users"].invoke
Rake::Task["db:seed:admins"].invoke
Rake::Task["db:seed:tasks"].invoke
Rake::Task["db:seed:task_requests_base"].invoke
Rake::Task["db:seed:task_requests_approve"].invoke
Rake::Task["db:seed:task_requests_complete"].invoke
Rake::Task["db:seed:reviews"].invoke
Rake::Task["db:seed:moneys"].invoke
Rake::Task["db:seed:tickets"].invoke
Rake::Task["db:seed:ticket_messages"].invoke