require 'rails_helper'
RSpec.describe Api::V1::TicketsController, type: :request do
  let! (:user) { create(:user) }
  let! (:other_user) { create(:user) }

  let! (:ticket) {create(:ticket, user_id: user.id)}
  let! (:ticket_params) { {ticket: attributes_for(:ticket)} }

  def get_tickets(user = nil)
    get api_v1_tickets_path, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def destroy_ticket(user, ticket_id)
    delete api_v1_ticket_path(ticket_id), params: {id: ticket_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def get_ticket(user, ticket_id)
    get api_v1_ticket_path(ticket_id), params: {id: ticket_id}, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  def create_ticket(user, params)
    post api_v1_tickets_path, params: params, headers: auth_headers(user)
    JSON.parse(response.body)
  end

  # index
  describe 'GET /api/v1/tickets' do
    describe 'positive scenarios' do
      it 'registred user can get his tickets' do
        json = get_tickets(user)
        expect(json['data']).to_not be_empty
      end

      it 'registred user can`t  get other user ticket' do
        other_user = create(:user)
        other_user_ticket = create(:ticket, user_id: other_user.id)

        json = get_tickets(user)
        expect(json['data'].size).to eq(1)
      end
    end

    describe 'negative scenarios' do
      it 'unregistred user can`t get his tickets' do
        json = get_tickets(nil)
        expect(response).to have_http_status(422)
      end
    end
  end

  # destroy
  describe 'DELETE /api/v1/ticket/:id/' do
    describe 'positive scenarios' do
      it 'registred user can delete his ticket' do
        json = destroy_ticket(user, ticket.id)
        expect(json['data']['attributes']['state']).to eq('deleted')
      end
    end

    describe 'negative scenarios' do
      it 'unregistred user can`t delete ticket' do
        json = destroy_ticket(nil, ticket.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t delete not his ticket' do
        ticket.update_attribute(:user_id, other_user.id)
        json = destroy_ticket(user, ticket.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t delete invalid ticket' do
        json = destroy_ticket(user, other_user.id)
        expect(response).to have_http_status(404)
      end

      it 'user can`t delete deleted ticket' do
        ticket.deleted!
        json = destroy_ticket(user, ticket.id)
        expect(response).to have_http_status(404)
      end
    end
  end


  # show
  describe 'GET /api/v1/ticket/:id/' do
    describe 'positive scenarios' do
      it 'registred user can show his ticket' do
        json = get_ticket(user, ticket)
        expect(json['data']['id']).to eq(ticket.id)
      end
    end

    describe 'negative scenarios' do
      it 'unregistred user can`t show ticket' do
        json = get_ticket(nil, ticket.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t show not his ticket' do
        ticket.update_attribute(:user_id, other_user.id)
        json = get_ticket(user, ticket.id)
        expect(response).to have_http_status(422)
      end

      it 'user can`t show invalid ticket' do
        json = get_ticket(user, 99)
        expect(response).to have_http_status(404)
      end

      it 'user can`t show deleted ticket' do
        ticket.deleted!
        json = get_ticket(user, ticket.id)
        expect(response).to have_http_status(404)
      end
    end
  end


  # create
  describe 'POST /api/v1/tickets/' do
    describe 'positive scenarios' do
      it 'registred user can create ticket' do
        json = create_ticket(user, ticket_params)
        expect(json['data']['attributes']['subject']).to eq(ticket_params[:ticket][:subject])
      end
    end

    describe 'negative scenarios' do
      it 'unregistred user can`t create ticket' do
        json = create_ticket(nil, ticket_params)
        expect(response).to have_http_status(422)
      end

      it 'user can`t create invalid tickets qty' do
        create_list(:ticket, 5, user_id: user.id)
        json = create_ticket(user, ticket_params)
        expect(response).to have_http_status(422)
      end

      it 'unregistred user can`t create for invalid params' do
        prm = ticket_params
        prm[:ticket][:subject] = ''
        json = create_ticket(nil, ticket_params)
        expect(response).to have_http_status(422)
      end
    end
  end
end
