module Categories
  class Index
    def call
      Category.roots.collect do |r_cat|
        {id: r_cat.id, type: 'category',
         attributes: {title: r_cat.title, root: true, children: get_children(r_cat)}
         }
      end
    end

    private

    def get_children(r_cat)
      r_cat.children.collect { |c_cat| {id: c_cat.id, parent_id: r_cat.id, title: c_cat.title, root: false} }
    end
  end
end