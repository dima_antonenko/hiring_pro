require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :request do
  let! (:city) { create(:city) }
  let! (:prm) { { email: Faker::Internet.email,
                  password: '1234567',
                  password_confirmation: '1234567',
                  name: 'Name',
                  phone: Faker::PhoneNumber.phone_number,
                  city_id: city.id,
                  accept_terms: '1'
          } }

  def sign_up(extra_params = {})
    prm.merge!(extra_params)
    post user_registration_path, params: {registration: prm}, headers: base_headers
    JSON.parse(response.body)
  end


  describe 'POST /api/v1/users' do
    describe 'positive scenarios' do
      it 'user can sign_up' do
        json = sign_up
        expect(response).to have_http_status(200)
      end
    end

    describe 'negative scenarios' do
      it 'user cant twice sign_up' do
        sign_up
        sign_up
        expect(response).to have_http_status(500)
      end

      it 'user cant sign_up if not accept_terms' do
        sign_up({accept_terms: '0'})
        expect(response).to have_http_status(500)
      end
    end
  end
end