module Avatars
  module Users
    class Create < Avatars::Base
      attr_reader :current_user, :image, :coordinates

      def initialize(current_user, image, coordinates)
        @current_user, @image, @coordinates = current_user, image, coordinates
      end

      def call
        validate!
        current_user.avatar.attach(image)
        check_and_save(current_user) do
          crop(current_user, coordinates)
        end
      end

      private

      def validate! # @todo дублируется в Tasks::Create & Tasks::Destroy
        image_exist?(image)
        user_active?(current_user)
      end
    end
  end
end
