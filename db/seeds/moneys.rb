MoneyLog.destroy_all

def add_logs(user)
  Moneys::Virtual.new(user, 100).increment
  Moneys::Virtual.new(user, 55).decrement

  Moneys::Real.new(user, 100).increment
  Moneys::Real.new(user, 55).decrement
end

User.all.each do |user|
  add_logs(user)
end

puts "Add moneys to users \n \n"