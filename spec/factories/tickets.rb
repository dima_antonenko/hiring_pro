FactoryBot.define do
  factory :ticket do
    subject { Faker::Lorem.sentence }
    description { Faker::Lorem.sentence(word_count: 10) }
    reason_type { 0 }
  end
end
