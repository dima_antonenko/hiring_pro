class Api::V1::PasswordsController < Devise::PasswordsController
  before_action :authenticate_user_from_token!, only: [:update]

  def update
  end
end