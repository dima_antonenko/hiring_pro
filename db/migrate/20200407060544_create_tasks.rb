class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks, id: :uuid do |t|
      t.string :title, null: false
      t.string :description
      t.string :service_location
      t.integer :price, null: false
      t.datetime :start_date
      t.datetime :completion_date, null: false

      t.integer :payment_type, index: true, null: false
      t.integer :state, index: true, null: false, default: 0
      t.boolean :deleted, index: true, null: false, default: false
      t.integer :category_id, index: true, null: false
      t.uuid    :user_id, index: true, null: false
      t.integer :city_id, index: true, null: false

      t.datetime :completed_at, index: true
      t.timestamps
    end
  end
end
