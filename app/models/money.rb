class Money < ApplicationRecord
  belongs_to :user
  has_many :money_logs

  self.table_name = "moneys"
end
