class Api::V1::CitiesController < ApplicationController
  def index
    result = City.order("position asc").all
    render_json CitySerializer.new(result) if stale?(result)
  end
end
