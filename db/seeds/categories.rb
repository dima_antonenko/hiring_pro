# rake db:seed:categories
cat = [

    [['Courier services',
      'Курьерские услуги'], [['Courier service', 'Услуги пешего курьера'],
                             ['Courier services in a passenger car', 'Услуги курьера на легковом авто'],
                             ['Buy and deliver', 'Купить и доставить'],
                             ['Express delivery', 'Срочная доставка'],
                             ['Delivery of products', 'Доставка продуктов'],
                             ['Food delivery from restaurants', 'Доставка еды из ресторанов'],
                             ['Daily courier', 'Курьер на день'],
                             ["Other", 'Другое']]],

    [['Repair and construction', 'Ремонт и строительство'],
     [['Master for an hour', 'Мастер на час'],
      ['Plumbing work', 'Сантехнические работы'],
      ['Electric installation work', 'Электромонтажные работы'],
      ['Finishing work', 'Отделочные работы'],
      ['The ceilings', 'Потолки'],
      ['Floors', 'Полы'],
      ['Tiled work', 'Плиточные работы'],
      ['Furniture assembly and repair', 'Сборка и ремонт мебели'],
      ['Installation and repair of doors and locks', 'Установка и ремонт дверей и замков'],
      ['Windows, glazing, balconies', 'Окна, остекление, балконы'],
      ["Roofing and front works", 'Кровельные и фасадные работы'],
      ["Heating, water supply, sewerage", 'Отопление, водоснабжение, канализация'],
      ["Insulation work", 'Изоляционные работы'],
      ["Construction and installation work", 'Строительно-монтажные работы'],
      ["Large construction", 'Крупное строительство'],
      ["Other", 'Другое']]],

    [['Cargo transportation', 'Грузоперевозки'],
     [["Transportation of things, transfers", 'Перевозка вещей, перезды'],
      ["Passenger Transportation", 'Пассажирские перевозки'],
      ["Building materials and equipment", 'Строительные грузы и оборудование'],
      ["Garbage removal", 'Вывоз мусора'],
      ["Tow trucks", 'Эвакуаторы'],
      ["Intercity transportation", 'Междугородные перевозки'],
      ["Loader services", 'Услуги грузчиков'],
      ["Food transportation", 'Перевозка продуктов'],
      ["Manipulator Services", 'Услуги манипулятора'],
      ["Other", 'Другое']]],

    [["Housekeeping and help", 'Уборка и помощь по хозяйству'],
     [["Spring-cleaning", 'Генеральная уборка'],
      ['Maintenance cleaning', 'Поддерживающая уборка'],
      ["Washing windows", 'Мытье окон'],
      ["Taking out the trash", 'Вынос мусора'],
      ["Seamstress help", 'Помощь швеи'],
      ["Cooking", 'Приготовление еды'],
      ["Ironing", 'Глажение белья'],
      ["Dry cleaning", 'Химчистка'],
      ["Animal care", 'Уход за животными'],
      ["Work in the garden, in the garden, on the site", 'Работы в саду, огороде, на участке'],
      ["Nurses", 'Сиделки'],
      ["Babysitting", 'Няни'],
      ["Other", 'Другое']]],

    [["Virtual assistant", 'Виртуальный помощник'],
     [["Work with text, copywriting, translations", 'Работа с текстом, копирайтинг, переводы'],
      ["Search and information processing", 'Поиск и обработка информации'],
      ["Work in MS Office", 'Работа в MS Office'],
      ["Decoding audio and video recordings", 'Расшифровка аудио и видео записей'],
      ["Ad Placement", 'Размещение объявлений'],
      ["Internet advertising and promotion", 'Реклама и продвижение в интернете'],
      ["Calling the base", 'Обзвон по базе'],
      ["Other", 'Другое']]],

    [["Computer help", 'Компьютерная помощь'],
     [["Repair of computers and laptops", 'Ремонт компьютеров и ноутбуков'],
      ["Installation and configuration of operating systems and programs", 'Установка и настройка операционных систем и программ'],
      ["Internet and WIFI setup", 'Настройка интернета и WIFI'],
      ["Repair and replacement of components", 'Ремонт и замена комлпектующих'],
      ["Data recovery", 'Восстановление данных'],
      ["Adjustment and repair of office equipment", 'Настройка и ремонт оргтехники'],
      ["Consultation and training", 'Консультация и обучение'],
      ["Other", 'Другое']]],

    [["Events and promotions", 'Мероприятия и промоакции'],
     [["Event help", 'Помощь в проведении мероприятий'],
      ["Distribution of promotional material", 'Раздача промо материала'],
      ["Mystery shopper", 'Тайный покупатель'],
      ["Handyman", 'Разнорабочий'],
      ["Promoter", 'Промоутер'],
      ["Toastmaster, host, animator", 'Тамада, ведущий, аниматор'],
      ["Promo Model", 'Промо модель'],
      ["Other", 'Другое']]],

    [["Design", 'Дизайн'],
     [["Corporate identity, logos and business cards", 'Фирменный стиль, логотипы и визитки'],
      ["Print design", 'Полиграфический дизайн'],
      ["Illustration painting, graffiti", 'Иллюстрация живопись, граффити'],
      ["Internet banners, design of social networks", 'Интернет-баннеры, оформление соц сетей'],
      ["3D graphics, animation", '3D графика, анимация'],
      ["Infographic, presentations, icons", 'Инфорграфика, презентации, иконки'],
      ["Outdoor advertising, stands", 'Наружная реклама, стенды'],
      ["Architecture, interior, landscape", 'Архитектура, интерьер, ландшафт'],
      ["Other", 'Другое']]],

    [["Web development", 'Web-разработка'],
     [["Turnkey website", 'Сайт под ключ'],
      ["Website development and support", 'Доработка и поддержка сайта'],
      ["Programming and software development", 'Программирование и разработка ПО'],
      ["Site layout", 'Верстка'],
      ["Mobile Application Development", 'Разработка мобильных приложений'],
      ["Other", 'Другое']]],

    [["Photo and video services", 'Фото и видеоуслуги'],
     [["Photography", 'Фотосъемка'],
      ["Video recording", 'Видеосъемка'],
      ["Retouching photos", 'Ретушь фотографий'],
      ["Turnkey video creation", 'Создание видеороликов под ключ'],
      ["Video editing and color grading", 'Монтаж и цветокоррекция видео'],
      ["Digitization", 'Оцифровка'],
      ["Other", 'Другое']]],

    [["Installation and repair of equipment", 'Установка и ремонт техники'],
     [["Refrigerators and freezers", 'Холодильники и морозильные камеры'],
      ["Washers and dryers", 'Стиральные и сушильные машины'],
      ["Dishwashers", 'Посудомоечные машины'],
      ["Electric cookers and panels", 'Электрические плиты и панели'],
      ["Gas stoves", 'Газовые плиты'],
      ["Ovens", 'Духовые шкафы'],
      ["Hoods", 'Вытяжки'],
      ["Air conditioning equipment", 'Климатическая техника'],
      ["Water heaters", 'Водонагреватели'],
      ["Sewing machines", 'Швейные машины'],
      ["Vacuum cleaners and cleaners", 'Пылесосы и очистители'],
      ["Irons and clothing care", 'Утюги и уход за одеждой'],
      ["Coffee machines", 'Кофемашины'],
      ["Microwave oven", 'СВЧ печи'],
      ["Small kitchen appliances", 'Мелкая кухонная техника'],
      ["Body and Health Care", 'Уход за телом и здоровьем'],
      ["Construction and garden equipment", 'Строительная и садовая техника'],
      ["Other", 'Другое']]],

    [["Beauty and health", 'Красота и здоровье'],
     [["Hairdressing services", 'Парикмахерские услуги'],
      ["Manicure", 'Ногтевой сервис'],
      ["Massage", 'Массаж'],
      ["Cosmetology, makeup", 'Косметология, макияж'],
      ["Stylists and image makers", 'Стилисты и имиджмейкеры'],
      ["Personal trainer", '"Персональный тренер'],
      ["Other", 'Другое']]],

    [["Digital Repair", 'Ремонт цифровой техники'],
     [["Tablets and Phones", 'Планшеты и телефоны'],
      ["Audio equipment and systems", 'Аудиотехника и систем'],
      ["TVs and monitors", 'Телевизоры и мониторы'],
      ["Car electronics", 'Автомобильная электроника'],
      ["Video / Photo Equipment", 'Видео/фототехника'],
      ["Gaming consoles", 'Видео/фототехника'],
      ["Antennas", 'Антенны'],
      ["Clock", 'Часы'],
      ["Other", 'Другое']]],

    [["Legal aid", 'Юридическая помощь'],
     [["Legal consultation", 'Юридическая консультация'],
      ["Drawing up and verification of contracts", 'Составление и проверка договоров'],
      ["Drawing up and filing complaints and claims", 'Составление и подача жалоб и исков'],
      ["Paperwork", 'Оформление документов'],
      ["Company registration and liquidation", 'Регистрация и ликвидация компании'],
      ["Other", 'Другое']]],

    [["Tutors and training", 'Репититоры и обучение'],
     [["Foreign languages", 'Иностранные языки'],
      ["Mathematics and physics", 'Математика и физика'],
      ["Biology and chemistry", 'Биология и химия'],
      ["Preparation for school", 'Подготовка к школе'],
      ["Music and dancing", 'Музыка и танцы'],
      ["Student assistance", 'Помощь студентам'],
      ["Speech therapists", 'Логопеды'],
      ["Sport", 'Спорт'],
      ["Car instructors", 'Автоинструкторы'],
      ["Other", 'Другое']]],

    [["Transport Repair", 'Ремонт транспорта'],
     [["Vehicle maintenance", 'Техническое обслуживание автомобиля'],
      ["Diagnostics and repair of engine and chassis", 'Диагностика и ремонт двигателя и ходовой части'],
      ["Body repair", 'Кузовной ремонт'],
      ["Auto electrician", 'Автоэлектрика'],
      ["Auto glass and tinting", 'Автостекла и тонировка'],
      ["Tire fitting", 'Шиномонтаж'],
      ["Washing and dry cleaning", 'Мойка и химчистка'],
      ["Tuning", 'Тюнинг'],
      ["Road assistance", 'Помощь на дороге'],
      ["Motoservice", 'Мотосервис'],
      ["Other", 'Другое']]]

]

def add_category(titles, position, root = nil)
  category = Category.create(title: titles[0], system_name: titles[1], position: position)
  category.move_to_child_of(root) if root
  category
end

Category.destroy_all
cat.each do |node|
  root_cat = add_category(node[0], cat.index(node))
  node[1].each do |children_cat_titles|
    add_category(children_cat_titles, node[1].index(children_cat_titles), root_cat)
  end
end

Category.roots.each do |root|
  puts "#{root.system_name} \n"
  root.children.each do |children|
    puts "-#{children.system_name}\n"
  end
end