@completed_task = Task.find_by(title: 'Completed task')
@task_creator = @completed_task.user
@request_creator = User.find_by(email: 'request_creator@test.ru')
@task_request = TaskRequest.where(task_id: @completed_task.id, user_id: @request_creator.id).first


result = TaskRequests::Approve.new(@task_creator, @task_request).call
puts "Approved task_request: #{result.attributes} for task: #{@completed_task.attributes} \n \n"

result = Tasks::Complete.new(@task_creator, @completed_task).call
puts "Completed task #{result.attributes} \n \n"