class TaskRequestSerializer
  include FastJsonapi::ObjectSerializer

  attributes :price, :message, :state, :created_at, :deleted, :task_id

  belongs_to :task
  belongs_to :user
end