module Avatars
  module Users
    class Update < Avatars::Base
      attr_reader :current_user, :coordinates #{x, y, width, height}

      def initialize(current_user, coordinates)
        @current_user, @coordinates = current_user, coordinates
      end

      def call
        validate!
        crop(current_user, coordinates)
        save_and_return(current_user)
      end

      private

      def validate!
        avatar_attached(current_user)
        user_active?(current_user)
      end
    end
  end
end
