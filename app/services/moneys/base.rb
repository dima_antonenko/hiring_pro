module Moneys
  class Base
    include UserValidations
    include MoneyValidations

    attr_reader :amount, :user, :commit

    def initialize(user, amount, commit = nil)
      @user, @amount, @commit = user, amount, commit
    end

    protected

    def update_score(money, operation_type = :increment, amount = 0, commit)
      amount_before = money.amount
      money.with_lock do
        money.send(operation_type, :amount, amount)
        money.save
        create_log(money, amount_before, money.amount, commit)
      end
    end

    def validate!
      user_active?(user)
      amount_valid?(amount)
    end

    private

    def create_log(money, amount_before, amount_after, commit)
      amount_diff = amount_after - amount_before
      MoneyLog.create({money_id: money.id,
                       amount_before: amount_before,
                       amount_after: amount_after,
                       amount_diff: amount_diff,
                       commit: commit})
    end
  end
end