FactoryBot.define do
  factory :user do
    email                 { Faker::Internet.email }
    password              { Faker::Internet.password(min_length: 8) }
    password_confirmation { password }
    name                  { Faker::Name.name }
    phone                 { Faker::PhoneNumber.phone_number }
    authentication_token  { Faker::Alphanumeric.alphanumeric(number: 20) }
    city
    # uid { user_email }
    # provider {'email'}
  end
end
