class Admin::ReviewsController < AdminController
  before_action :get_review, only: [:edit, :update, :destroy]

  def index
    @reviews = Review.all.includes(:task, :user)
  end

  def edit; end

  def update
    @review.assign_attributes(review_params)
    @review.save ? flash[:success] = "Review will be updated" : flash[:errors] = @review.errors.full_messages
    redirect_back(fallback_location: admin_reviews_url)
  end

  def destroy
    @review.update_attribute(:deleted, true)
    redirect_back(fallback_location: admin_reviews_url)
  end

  private

  def review_params
    params.require(:review).permit( :description)
  end

  def get_review
    @review = Review.find(params[:id])
  end
end