module Tickets
  class Show < BaseService
    include UserValidations
    include TicketValidations

    attr_accessor :current_user, :ticket

    def initialize(current_user, ticket)
      @current_user, @ticket = current_user, ticket
    end

    def call
      validate!
      ticket
    end

    private

    def validate!
      user_active?(current_user)
      ticket_active?(ticket)
      user_related_to_ticket?(ticket, current_user)
    end
  end
end