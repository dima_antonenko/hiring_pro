class CurrentUserSerializer
  include FastJsonapi::ObjectSerializer

  attributes :name, :email, :phone

  # belongs_to :city
  # has_many :tasks
  # has_many :reviews, foreign_key: "recipient_id"

  attribute :real_money do |user|
    user.real_money.amount
  end

  attribute :virtual_money do |user|
    user.virtual_money.amount
  end

  attribute :avatar_full_path do |user|
    if user.avatar.attached?
      Rails.application.routes.url_helpers.rails_blob_path(user.avatar, only_path: true)
    else
      nil
    end
  end
end
