module Reviews
  class Destroy < BaseService
    include ReviewValidations
    include UserValidations

    attr_reader :current_user, :review

    def initialize(current_user, review)
      @current_user, @review = current_user, review
    end

    def call
      validate!
      review.update_attribute(:deleted, true)
      decrease_rating(review.user, review.rating)
      review
    end

    private

    def validate!
      user_active?(current_user)
      review_has_valid_state?(review)
      user_related_with_review?(current_user, review)
      review_has_valid_time?(review)
    end
  end
end