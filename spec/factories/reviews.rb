FactoryBot.define do
  factory :review do
    description { Faker::Lorem.sentence(word_count: 10) }
    rating { 5 }
  end
end
