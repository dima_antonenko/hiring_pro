class CreateScoreLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :money_logs, id: :uuid do |t|
      t.uuid :money_id,    index: true

      t.decimal :amount_before, null: false, default: 0
      t.decimal :amount_after,  null: false, default: 0

      t.decimal :amount_diff,   null: false, default: 0
      t.timestamps
      t.text :commit
    end
    add_index :money_logs, :created_at
    add_index :money_logs, :updated_at
  end
end
