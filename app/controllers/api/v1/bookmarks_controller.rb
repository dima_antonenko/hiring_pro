class Api::V1::BookmarksController < ApplicationController
  before_action :authenticate_user_from_token!

  def create
    result = Bookmarks::Base.new(current_user, prm[:item_type], prm[:item_id]).create
    render_response(result)
  end

  def destroy
    result = Bookmarks::Base.new(current_user, prm[:item_type], prm[:item_id]).destroy
    render_response(result)
  end

  private

  def prm
    params.permit(:item_type, :item_id)
  end
end
