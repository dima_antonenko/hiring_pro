class Admin::UsersController < AdminController
  before_action :get_user, only: [:edit, :update, :destroy, :real_money, :virtual_money]

  def index
    @users = User.all.includes(:tasks, :reviews, :city)
  end

  def edit; end

  def update
    @user.assign_attributes(review_params)
    @user.save ? flash[:success] = "Review will be updated" : flash[:errors] = @user.errors.full_messages
    redirect_back(fallback_location: admin_users_url)
  end

  def destroy
    @user.update_attribute(:deleted, true)
    redirect_back(fallback_location: admin_users_url)
  end

  def real_money
    @records = @user.real_money.money_logs.order('created_at DESC')
  end

  def virtual_money
    @records = @user.virtual_money.money_logs.order('created_at DESC')
  end

  private

  def review_params
    params.require(:user).permit(:name)
  end

  def get_user
    @user = User.find(params[:id])
  end
end